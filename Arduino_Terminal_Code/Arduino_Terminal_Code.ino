
#include <SoftwareSerial.h>
SoftwareSerial BTSerial(4,5); 

int terminal_difficulty_level, level_hacked, step_pin_OUT, step_pin_IN, alarm_pin;
bool button_pressed = false;
void setup() {
  terminal_difficulty_level = 1;
  level_hacked = 1;
  step_pin_OUT = 10;
  step_pin_IN = A3;
  alarm_pin = 9;
  
  pinMode(alarm_pin, OUTPUT);
  pinMode(13, OUTPUT);    
  digitalWrite(alarm_pin, LOW);
  digitalWrite(13, LOW);
  pinMode(step_pin_OUT, OUTPUT);
  pinMode(step_pin_IN, INPUT);
  digitalWrite(step_pin_OUT, LOW);
  digitalWrite(step_pin_OUT, HIGH);
  
  String setName = String("AT+NAME=Terminal_1\r\n"); //Setting name as 'Terminal_1'
  Serial.begin(9600);
  BTSerial.begin(9600);
}
void loop() {
  digitalWrite(13, LOW);
  // reading button press and changing difficulty level 
  //Serial.println(terminal_difficulty_level);
  if (button_pressed == false && digitalRead(step_pin_IN) == HIGH) { // User depressed button
    button_pressed = true;
  }
  else  if (button_pressed == true && digitalRead(step_pin_IN) == LOW) { // User released button
    button_pressed = false; // Resets button

    // Resets hack level and increments difficulty level
    level_hacked = 1;
    terminal_difficulty_level++;
    if (terminal_difficulty_level > 4) {
      terminal_difficulty_level = 1;
    }
    
    // Sends a signal here about levels being reset
    BTSerial.print("Difficulty_Level_");
    BTSerial.print(terminal_difficulty_level);
    BTSerial.print("_Access_Level_");
    BTSerial.println(level_hacked);
    // BTSerial.println("_END");
    
    
    // Send visual confirmation of difficulty level change
    digitalWrite(13, HIGH);
    delay(1000 * terminal_difficulty_level);
    digitalWrite(13, LOW);
    
  }
  bool data_received = false;
  while (BTSerial.available()) {
    data_received = true;
    int received_access_level = int(BTSerial.parseInt());
    Serial.println(received_access_level);

    // Checks whether this is a legitimate access or an alert signal
    if (received_access_level == 0) {
      // Set off alarm
      analogWrite(alarm_pin, 90);
      delay(2000);
      analogWrite(alarm_pin, 0);
    }
    else {    
      // Sets hack level and increments difficulty level
      if (received_access_level > level_hacked) {
        level_hacked = received_access_level;
      }
      
      
      // Sends a signal here about levels being reset
      BTSerial.print("Difficulty_Level_");
      BTSerial.print(terminal_difficulty_level);
      BTSerial.print("_Access_Level_");
      BTSerial.println(level_hacked);
    }
  }
  // delay(1000);
}
