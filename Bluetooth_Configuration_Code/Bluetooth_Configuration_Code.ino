
#include <SoftwareSerial.h>
SoftwareSerial BTSerial(4,5); 
void setup() {
  pinMode(7, OUTPUT);            // Setting pin $7 high to enable congiuration of the bluetooth module.
  digitalWrite(7, HIGH);
  
  String setName = String("AT+NAME=MyBTBee\r\n"); //Setting name as 'MyBTBee'
  Serial.begin(9600);
  BTSerial.begin(38400);
  BTSerial.print("AT\r\n"); //Check Status
  delay(500);
  while (BTSerial.available()) {
      Serial.write(BTSerial.read());
    }
  BTSerial.print(setName); //Send Command to change the name
  delay(500);
  while (BTSerial.available()) {
      Serial.write(BTSerial.read());
    }}
void loop() {}
