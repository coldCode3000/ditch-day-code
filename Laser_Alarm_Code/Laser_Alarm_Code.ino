// Code for Warframe Laser Alarm

#define SPEAKER_PWM 25

// Button Variables
bool button_pressed = false;
int step_pin_OUT, step_pin_IN;
int difficulty_level = 1;

// Pins and variables for laser
bool laser_on[3], laser_contact[3];
int laser_in[3], laser_control[3];
int laser_templates[24][3];
int laser_pattern_1[3];
int laser_pattern_2[3];
int true_pattern[3];
int laser_counter[3];
int laser_steps_reached[3];
int laser_buffer[3];
bool synced[3];

// Pins and variables for interface
int indicator_light;

// Pins and variables for speaker
int speaker_out;
int speaker_pwm;

// Alarm variables
// These determine whether or not to trigger the alarm
int alarm_length; // Determines how long an alarm runs for
int beep_length; // Determines length of beeps (in milliseconds)
int off_length; // Determines length of time alarm is silent in between beeps (in milliseconds)
int time_passed; // Checks how lmuch time has passed
bool alarm_active; // Answers whether or not the alarm has been triggered
bool alarm_beeping; // Answers whether or not the alarm is making a noise
int time_step; // Counts time spent in a state, either beeping or not
int time_active; // Counts time since alarm was triggered

void setup() {

  Serial.begin(115200);

  // Initializing laser pins and variables
  laser_contact[0] = false; // Bool for determining whether or not laser 1 has found target
  laser_contact[1] = false; // Bool for determining whether or not laser 2 has found target
  laser_contact[2] = false; // Bool for determining whether or not laser 3 has found target
  laser_in[0] = 6; // Signal IN pin from laser 1
  laser_in[1] = 7; // Signal IN pin from laser 2
  laser_in[2] = 8; // Signal IN pin from laser 3
  laser_on[0] = true;
  laser_on[1] = true;
  laser_on[2] = true;
  laser_control[0] = 3; // Signal OUT pin to laser 1
  laser_control[1] = 4; // Signal OUT pin to laser 2
  laser_control[2] = 5; // Signal OUT pin to laser 3
  laser_pattern_1[0] = 3; // Number of on stops before a full off-step for laser 1
  laser_pattern_1[1] = 2; // Number of on stops before a full off-step for laser 2
  laser_pattern_1[2] = 3; // Number of on stops before a full off-step for laser 3
  laser_pattern_2[0] = 3; // Number of on stops before a full off-step for laser 1
  laser_pattern_2[1] = 2; // Number of on stops before a full off-step for laser 2
  laser_pattern_2[2] = 3; // Number of on stops before a full off-step for laser 3
  true_pattern[0] = 3; // Number of on stops before a full off-step for laser 1
  true_pattern[1] = 2; // Number of on stops before a full off-step for laser 2
  true_pattern[2] = 3; // Number of on stops before a full off-step for laser 3
  laser_counter[0] = 0; // Keeps track of time for laser 1, in milliseconds
  laser_counter[1] = 0; // Keeps track of time for laser 2, in milliseconds
  laser_counter[2] = 0; // Keeps track of time for laser 3, in milliseconds
  laser_steps_reached[0] = 0; // Keeps track of how many off/on cycles have been completed for laser 1
  laser_steps_reached[1] = 0; // Keeps track of how many off/on cycles have been completed for laser 2
  laser_steps_reached[2] = 0; // Keeps track of how many off/on cycles have been completed for laser 3
  laser_buffer[0] = 0; // Buffer to keep signal from laser 1 from messing up stuff
  laser_buffer[1] = 0; // Buffer to keep signal from laser 2 from messing up stuff
  laser_buffer[2] = 0; // Buffer to keep signal from laser 3 from messing up stuff

  synced[0] = false;
  synced[1] = false;
  synced[2] = false;

  laser_templates[0][0] = 3;
  laser_templates[0][1] = 2;
  laser_templates[0][2] = 3;
  
  laser_templates[1][0] = 2;
  laser_templates[1][1] = 3;
  laser_templates[1][2] = 2;
  
  laser_templates[2][0] = 3;
  laser_templates[2][1] = 3;
  laser_templates[2][2] = 2;
  
  laser_templates[3][0] = 2;
  laser_templates[3][1] = 2;
  laser_templates[3][2] = 3;
  
  laser_templates[4][0] = 3;
  laser_templates[4][1] = 2;
  laser_templates[4][2] = 2;
  
  laser_templates[5][0] = 2;
  laser_templates[5][1] = 3;
  laser_templates[5][2] = 3;
  
  laser_templates[6][0] = 2;
  laser_templates[6][1] = 3;
  laser_templates[6][2] = 4;
  
  laser_templates[7][0] = 3;
  laser_templates[7][1] = 2;
  laser_templates[7][2] = 4;
  
  laser_templates[8][0] = 4;
  laser_templates[8][1] = 3;
  laser_templates[8][2] = 2;
  
  laser_templates[9][0] = 4;
  laser_templates[9][1] = 2;
  laser_templates[9][2] = 3;
  
  laser_templates[10][0] = 3;
  laser_templates[10][1] = 4;
  laser_templates[10][2] = 2;
  
  laser_templates[11][0] = 2;
  laser_templates[11][1] = 4;
  laser_templates[11][2] = 3;
  
  laser_templates[12][0] = 1;
  laser_templates[12][1] = 3;
  laser_templates[12][2] = 4;
  
  laser_templates[13][0] = 3;
  laser_templates[13][1] = 2;
  laser_templates[13][2] = 3;
  
  laser_templates[14][0] = 4;
  laser_templates[14][1] = 3;
  laser_templates[14][2] = 1;
  
  laser_templates[15][0] = 2;
  laser_templates[15][1] = 3;
  laser_templates[15][2] = 2;
  
  laser_templates[16][0] = 4;
  laser_templates[16][1] = 2;
  laser_templates[16][2] = 3;
  
  laser_templates[17][0] = 3;
  laser_templates[17][1] = 1;
  laser_templates[17][2] = 4;
  
  laser_templates[18][0] = 5;
  laser_templates[18][1] = 1;
  laser_templates[18][2] = 3;
  
  laser_templates[19][0] = 3;
  laser_templates[19][1] = 4;
  laser_templates[19][2] = 2;
  
  laser_templates[20][0] = 4;
  laser_templates[20][1] = 3;
  laser_templates[20][2] = 1;
  
  laser_templates[21][0] = 1;
  laser_templates[21][1] = 3;
  laser_templates[21][2] = 5;
  
  laser_templates[22][0] = 2;
  laser_templates[22][1] = 2;
  laser_templates[22][2] = 3;
  
  laser_templates[23][0] = 3;
  laser_templates[23][1] = 1;
  laser_templates[23][2] = 5;

  pinMode(laser_in[0], INPUT);
  pinMode(laser_in[1], INPUT);
  pinMode(laser_in[2], INPUT);

  pinMode(laser_control[0], OUTPUT);
  pinMode(laser_control[1], OUTPUT);
  pinMode(laser_control[2], OUTPUT);

  // Initializing interface pins and variables
  indicator_light = 13;
  step_pin_OUT = 10;
  step_pin_IN = A3;
  pinMode(step_pin_OUT, OUTPUT);
  pinMode(step_pin_IN, INPUT);
  digitalWrite(step_pin_OUT, LOW);
  digitalWrite(step_pin_OUT, HIGH);

  pinMode(indicator_light, OUTPUT);

  // Initializing speaker pins and variables
  speaker_out = 9;
  speaker_pwm = SPEAKER_PWM;

  pinMode(speaker_out, OUTPUT);


  alarm_length = 5000;
  beep_length = 500;
  off_length = 250;
  time_passed = millis();
  time_active = 0;
  time_step = 0;
  alarm_active = false;
  alarm_beeping = false;

}

void loop() {
  digitalWrite(13, LOW);
  // reading button press and changing difficulty level 
  //Serial.println(terminal_difficulty_level);
  if (button_pressed == false && digitalRead(step_pin_IN) == HIGH) { // User depressed button
    button_pressed = true;
  }
  else  if (button_pressed == true && digitalRead(step_pin_IN) == LOW) { // User released button
    button_pressed = false; // Resets button

    // Resets hack level and increments difficulty level
    difficulty_level++;
    if (difficulty_level > 4) {
      difficulty_level = 1;
    }

    // Reset everything
    for (int i = 0; i < 3; i++) {
      digitalWrite(laser_control[i], HIGH);
      laser_on[i] = true;
      laser_steps_reached[i] = 0;
      laser_buffer[i] = 0;
    }

    // Set new laser patterns
    int random_1 = 6 * (difficulty_level - 1) + random(6);
    int random_2 = random_1; // Works for first two difficulties
    if (difficulty_level > 2) {
      while (random_2 == random_1) { // At difficulties > 2, randomizing both patterns and NOT allowing them to be the same.
      random_2 = 6 * (difficulty_level - 1) + random(6);
      }
    }
    for (int k = 0; k < 3; k++) {
      laser_pattern_1[k] = laser_templates[random_1][k];
      laser_pattern_2[k] = laser_templates[random_2][k];
      // Setting true pattern to be the same as the first one
      true_pattern[k] = laser_templates[random_1][k];
    }
    
    // Send visual confirmation of difficulty level change
    digitalWrite(13, HIGH);
    delay(1000 * difficulty_level);
    digitalWrite(13, LOW);
    
  }
  for (int i = 0; i < 3; i++) {
    synced[i] = false;
    if (laser_counter[i] < (2000 - 125)) {
      if (laser_steps_reached[i] < laser_pattern_1[i]) {
        digitalWrite(laser_control[i], HIGH);
        laser_on[i] = true;
      }
      else {
        digitalWrite(laser_control[i], LOW);
        laser_on[i] = false;
      }
    }
    else if (laser_counter[i] >= (2000 - 125) && laser_counter[i] < (2000)) {
      digitalWrite(laser_control[i], LOW);
      laser_on[i] = false;
    }
    else {
      laser_counter[i] = 0;
      laser_steps_reached[i]++;
      if (laser_steps_reached[i] >= laser_pattern_1[i] + 1) {
        digitalWrite(laser_control[i], HIGH);
        laser_on[i] = true;
        laser_steps_reached[i] = 0;
        laser_buffer[i] = 0;
        synced[i] = true;
      }
      else if (laser_steps_reached[i] < laser_pattern_1[i]) {
        laser_buffer[i] = 0;
      }
    }
  }
  if (synced[0] == true && synced[1] == true && synced [2] == true) {
    for (int k = 0; k < 3; k++) {
      if (difficulty_level < 4) {
        if (true_pattern[0] == laser_pattern_1[0] &&
        true_pattern[1] == laser_pattern_1[1] && 
        true_pattern[2] == laser_pattern_1[2]) { 
          // Switch to Pattern #2
          true_pattern[k] = laser_pattern_2[k];
        }
        else { 
          // Switch to Pattern #1
          true_pattern[k] = laser_pattern_1[k];
        }
      }
      else { // Max Difficulty
        // Switch randomly between patterns
        if (random(2) < 1) { 
          // Switch to Pattern #2
          true_pattern[k] = laser_pattern_2[k];
        }
        else { 
          // Switch to Pattern #1
          true_pattern[k] = laser_pattern_1[k];
        }
      }
    }
  }




  if (alarm_active == true && time_active > alarm_length) {
    alarm_active = false;
    alarm_beeping = false;
    time_step = 0;
    time_active = 0;
  }

  // Determining whether or not each laser has found a target
  for (int i = 0; i < 3; i++) {
    if (digitalRead(laser_in[i]) == LOW && laser_on[i] == true && laser_buffer[i] > 15) {
      laser_contact[i] = false;
    }
    else {
      laser_contact[i] = true;
    }
  }


  // Turning on light if any laser is detecting contact
  if (laser_contact[0] == false || laser_contact[1] == false || laser_contact[2] == false) {
    digitalWrite(indicator_light, HIGH);
    if (alarm_active == false) {
      alarm_active = true;      // Turns on alarm only if not currently in middle of alarm
    }
  }
  else {
    digitalWrite(indicator_light, LOW);
  }

  if (alarm_active == true) {
    if (alarm_beeping == true && time_step < beep_length) {
      speaker_pwm = SPEAKER_PWM;
    }
    else if (alarm_beeping == true && time_step >= beep_length) {
      speaker_pwm = 0;
      time_step = 0;
      alarm_beeping = false;
    }
    else if (alarm_beeping == false && time_step < off_length) {
      speaker_pwm = 0;
    }
    else {
      speaker_pwm = SPEAKER_PWM;
      time_step = 0;
      alarm_beeping = true;
    }
  }
  else {
    alarm_beeping = false;
    speaker_pwm = 0;
  }
  analogWrite(speaker_out, speaker_pwm);

  // Always reset laser_contact to false at the end of each cycle
  // Also update time_passed
  if (alarm_active == true) {
    time_step += (millis() - time_passed);
    time_active += (millis() - time_passed);
  }
  for (int i = 0; i < 3; i++) {
    laser_counter[i] += (millis() - time_passed);
    laser_buffer[i] += (millis() - time_passed);
    laser_contact[i] = false;
  }
  time_passed = millis();

  Serial.print("what is time_passed: ");
  Serial.println(time_passed);
}
