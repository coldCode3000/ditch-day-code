
#include <SoftwareSerial.h>
SoftwareSerial BTSerial(4,5); 

int terminal_difficulty_level, level_hacked, step_pin_OUT, step_pin_IN, alarm_pin, pulse_length, current_time, time_counter;
bool button_pressed = false;
void setup() {
  terminal_difficulty_level = 1;
  level_hacked = 1;
  step_pin_OUT = 10;
  step_pin_IN = A3;
  alarm_pin = 9;
  current_time = 0;
  time_counter = 0;
  
  pinMode(alarm_pin, OUTPUT);
  pinMode(13, OUTPUT);    
  digitalWrite(alarm_pin, LOW);
  digitalWrite(13, LOW);
  pinMode(step_pin_OUT, OUTPUT);
  pinMode(step_pin_IN, INPUT);
  digitalWrite(step_pin_OUT, LOW);
  digitalWrite(step_pin_OUT, HIGH);
  
  String setName = String("AT+NAME=Terminal_1\r\n"); //Setting name as 'Terminal_1'
  Serial.begin(9600);
  BTSerial.begin(9600);
}
void loop() {
  digitalWrite(13, LOW);
  
  bool data_received = false;
  while (BTSerial.available()) {
    data_received = true;
    int received_access_level = int(BTSerial.parseInt());
    Serial.println(received_access_level);

    // Checks whether this is a legitimate access or an alert signal
    if (received_access_level == 0) {
      // set servo to one side;
      pulse_length = 570;
      analogWrite(alarm_pin, 80);
    }
    else if (received_access_level == 1){    
      // Set servo to middle position;
      pulse_length = 1500;
      analogWrite(alarm_pin, 150);
    }
    else {
      pulse_length = 2400;
      analogWrite(alarm_pin, 230);
    }
    /*int total_time = micros();
    current_time = micros() - time_counter;
    while (current_time - total_time < 30000) {
      current_time = micros() - time_counter;
      if (current_time < pulse_length) {
        digitalWrite(alarm_pin, HIGH);
      }
      else if (current_time >= pulse_length && current_time < 2400) {
        digitalWrite(alarm_pin, LOW);
      }
      else { //if (current_time >= pulse_length && current_time >= 2400) {
        digitalWrite(alarm_pin, HIGH);
        time_counter = micros();
      }
    }
    digitalWrite(alarm_pin, LOW);*/
  }
  
  
}
