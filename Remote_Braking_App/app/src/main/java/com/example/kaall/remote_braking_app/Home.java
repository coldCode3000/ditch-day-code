package com.example.kaall.remote_braking_app;

import android.animation.ValueAnimator;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.util.Date;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import static android.view.View.INVISIBLE;
import static java.lang.Integer.parseInt;

public class Home extends AppCompatActivity {
    final String Terminal_1_ADDRESS="20:16:09:08:18:94";
    final String Terminal_2_ADDRESS="20:16:11:02:03:84";
    final String Terminal_3_ADDRESS="20:16:12:12:70:07";
    final String Terminal_4_ADDRESS="20:16:12:12:69:07";
    private final UUID PORT_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");//Serial Port Service ID
    private BluetoothDevice device;
    private BluetoothSocket socket;
    private OutputStream outputStream;
    private InputStream inputStream;



    String decode_string = ""; // string for receiving and decoding signals from the Arduino terminals
    Boolean start_pressed = false;
    int start_counter = 0;
    int current_terminal = 1;
    Button startButton, stopButton, brakeButton, antitipButton, storageButton;
    TextView textView, hack_timer_text;

    int[] terminal_security_level = new int[4];
    int current_hack_level = 0;
    ImageView[] locks;
    ImageView[] unlocks;
    boolean hack_instance_completed = false;


    float download_progress = 0;
    boolean downloading = false;
    int current_terminal_difficulty_level = 1;
    int current_terminal_access_level = 1;
    int practice_level = 1;
    boolean practicing = false;

    double temp_hack_time;
    double true_hack_time;
    boolean hack_time_set;
    String temp_timer_text;

    float progress_degrees, pick_direction;
    EditText editText;
    boolean deviceConnected=false;
    Thread thread;
    byte buffer[];
    int bufferPosition;
    boolean stopThread;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        startButton = (Button) findViewById(R.id.buttonConnect);
        stopButton = (Button) findViewById(R.id.buttonDisconnect);
        brakeButton = (Button) findViewById(R.id.buttonBrake);
        antitipButton = (Button) findViewById(R.id.buttonAntiTip);
        storageButton = (Button) findViewById(R.id.buttonStorage);

        stopButton.setEnabled(false);
        brakeButton.setEnabled(false);
        antitipButton.setEnabled(false);
        storageButton.setEnabled(false);


        final ValueAnimator animator = ValueAnimator.ofFloat(0.0f, 1.0f);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setInterpolator(new LinearInterpolator());
        animator.setDuration(10000L);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {

                // Running bluetooth coneect stuff
                if (start_pressed == true && start_counter < 1) {
                    start_counter++;
                }
                else if (start_pressed == true && start_counter >= 1) {
                    attempt_connection();
                    start_pressed = false;
                    start_counter = 0;
                }
            }
        });
        animator.start();

    }

    public void setUiEnabled(boolean bool)
    {
        startButton.setEnabled(!bool);
        stopButton.setEnabled(bool);

    }

    public void saveData() {
        // Open save_file right there
        SharedPreferences save_file = getApplicationContext().getSharedPreferences("Warframe_Vaykor_Cipher_Save", 0);
        SharedPreferences.Editor savior = save_file.edit();

        if (current_terminal == 1) { //  AEDF terminal
            savior.putInt("AEDF_access_level", current_terminal_access_level);
            savior.putInt("AEDF_difficulty_level", current_terminal_difficulty_level);
        }
        else if (current_terminal == 2) { //  DDF terminal
            savior.putInt("DDF_access_level", current_terminal_access_level);
            savior.putInt("DDF_difficulty_level", current_terminal_difficulty_level);
        }
        else if (current_terminal == 3) { //  EWF terminal
            savior.putInt("EWF_access_level", current_terminal_access_level);
            savior.putInt("EWF_difficulty_level", current_terminal_difficulty_level);
        }
        else { //  IRF terminal
            savior.putInt("IRF_access_level", current_terminal_access_level);
            savior.putInt("IRF_difficulty_level", current_terminal_difficulty_level);
        }

        savior.apply();
    }


    public void Send() {
        // Sends guess at access level to Arduino
        // If Android's access level is legitimately higher than the Arduino's, Arduino's signal back
        //   will reflect that
        String output_signal = String.valueOf(current_terminal_access_level);
        output_signal.concat("\n");
        try {
            outputStream.write(output_signal.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        current_terminal_access_level = 1; // Reset the access level temporarily; reply from Arduino will update it
    }

    public void Send_Brake_signal(View view) {
        // Sends alert signal to Arduino; doesn't expect a signal back in Bluetooth
        String output_signal = String.valueOf("0");
        output_signal.concat("\n");
        try {
            outputStream.write(output_signal.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Toast.makeText(getApplicationContext(),"0",Toast.LENGTH_SHORT).show();
    }


    public void Send_Anti_Tip_signal(View view) {
        // Sends alert signal to Arduino; doesn't expect a signal back in Bluetooth
        String output_signal = String.valueOf("1");
        output_signal.concat("\n");
        try {
            outputStream.write(output_signal.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Toast.makeText(getApplicationContext(),"1",Toast.LENGTH_SHORT).show();
    }

    public void Send_Storage_signal(View view) {
        // Sends alert signal to Arduino; doesn't expect a signal back in Bluetooth
        String output_signal = String.valueOf("2");
        output_signal.concat("\n");
        try {
            outputStream.write(output_signal.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Toast.makeText(getApplicationContext(),"2",Toast.LENGTH_SHORT).show();
    }

    public boolean BTverify(BluetoothDevice temp_device)
    {
        boolean connected=true;
        try {
            socket = temp_device.createRfcommSocketToServiceRecord(PORT_UUID);
            socket.connect();
        } catch (IOException e) {
            e.printStackTrace();
            connected=false;
        }
        if(connected)
        {
            try {
                outputStream=socket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                inputStream=socket.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }


        return connected;
    }

    public boolean BTinit()
    {
        boolean found=false;
        BluetoothAdapter bluetoothAdapter=BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            Toast.makeText(getApplicationContext(),"Device doesn't Support Bluetooth",Toast.LENGTH_SHORT).show();
        }
        if(!bluetoothAdapter.isEnabled())
        {
            Intent enableAdapter = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableAdapter, 0);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Set<BluetoothDevice> bondedDevices = bluetoothAdapter.getBondedDevices();
        if(bondedDevices.isEmpty())
        {
            Toast.makeText(getApplicationContext(),"Please Pair Security Terminal First",Toast.LENGTH_SHORT).show();
        }
        else
        {
            for (BluetoothDevice iterator : bondedDevices)
            {
                //if(iterator.getName().equals("Terminal_1"))
                String temp_address = iterator.getAddress();
                if((temp_address.equals(Terminal_1_ADDRESS) && BTverify(iterator))
                        || (temp_address.equals(Terminal_2_ADDRESS) && BTverify(iterator))
                        || (temp_address.equals(Terminal_3_ADDRESS) && BTverify(iterator))
                        || (temp_address.equals(Terminal_4_ADDRESS) && BTverify(iterator)))
                {
                    if (temp_address.equals(Terminal_1_ADDRESS)) {
                        current_terminal = 1;
                    }
                    else if (temp_address.equals(Terminal_2_ADDRESS)) {
                        current_terminal = 2;
                    }
                    else if (temp_address.equals(Terminal_3_ADDRESS)) {
                        current_terminal = 3;
                    }
                    else {
                        current_terminal = 4;
                    }
                    // Send(); // Sends data to Arduino, which will trigger a response that will set the levels for this terminal
                    device=iterator;
                    found=true;
                    stopButton.setEnabled(true);
                    brakeButton.setEnabled(true);
                    antitipButton.setEnabled(true);
                    storageButton.setEnabled(true);
                    //Toast.makeText(getApplicationContext(),iterator.getAddress(),Toast.LENGTH_SHORT).show();
                    break;
                }
                /*Toast.makeText(getApplicationContext(),iterator.getName(),Toast.LENGTH_SHORT).show();
                device=iterator;
                found=true;
                break;*/
            }
            if (found == false) {
                Toast.makeText(getApplicationContext(),"No Paired Terminals in Range",Toast.LENGTH_SHORT).show();
            }
        }
        return found;
    }

    public boolean BTconnect()
    {
        boolean connected=true;
        try {
            socket = device.createRfcommSocketToServiceRecord(PORT_UUID);
            socket.connect();
        } catch (IOException e) {
            e.printStackTrace();
            connected=false;
        }
        if(connected)
        {
            try {
                outputStream=socket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                inputStream=socket.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }


        return connected;
    }

    public void attempt_connection() {
        if(BTinit())
        {
            /*if(BTconnect())
            {
                setUiEnabled(true);
                deviceConnected=true;
                beginListenForData();
                textView.append("\nConnection Opened!\n");
            }*/
            setUiEnabled(true);
            deviceConnected=true;
            beginListenForData();
            Send();
        }
        else {
            startButton.setEnabled(true);
            start_pressed = false;
            start_counter = 0;
        }
    }

    public void onClickStart(View view) {
        start_pressed = true;
        startButton.setEnabled(false);
    }

    void beginListenForData()
    {
        final Handler handler = new Handler();
        stopThread = false;
        buffer = new byte[1024];
        Thread thread  = new Thread(new Runnable()
        {
            public void run()
            {
                while(!Thread.currentThread().isInterrupted() && !stopThread)
                {
                    try
                    {
                        int byteCount = inputStream.available();
                        if(byteCount > 0)
                        {
                            byte[] rawBytes = new byte[byteCount];
                            inputStream.read(rawBytes);
                            final String string=new String(rawBytes,"UTF-8");
                            //String new_string = decode_string.concat(string);
                            decode_string = decode_string.concat(string);
                            if (decode_string.contains("\n")) {
                                // Print out string twice
                                handler.post(new Runnable() {
                                    public void run()
                                    {
                                        // Printing out received data
                                        // textView.append(decode_string);
                                        // Finding the numbers for difficulty and access level
                                        int difficulty_index = decode_string.indexOf("Difficulty_Level_");
                                        int access_index = decode_string.indexOf("Access_Level_");

                                        // Updating the textview
                                        textView.setText("\nConnected to ");
                                        if (current_terminal == 1) {
                                            textView.append("AEDF Data Terminal.\n");
                                        }
                                        else if (current_terminal == 2) {
                                            textView.append("DDF Data Terminal.\n");
                                        }
                                        else if (current_terminal == 3) {
                                            textView.append("EWF Data Terminal.\n");
                                        }
                                        else { //if (current_terminal == 4) {
                                            textView.append("IRF Data Terminal.\n");
                                        }
                                        textView.append ("\nEncryption Level: ");
                                        textView.append(String.valueOf(current_terminal_difficulty_level));
                                        textView.append("\n");

                                        if (current_terminal_access_level >= 9) {
                                            textView.append("\nAll 8 files recovered.\n");
                                        }
                                        else {
                                            textView.append ("\n");
                                            textView.append(String.valueOf(current_terminal_access_level - 1));
                                            textView.append(" / 8 files accessed.\n");
                                        }
                                        decode_string = ""; // Reset decoder

                                    }
                                });
                            }

                        }
                    }
                    catch (IOException ex)
                    {
                        stopThread = true;
                    }
                }
            }
        });

        thread.start();
    }

    public void onClickStop(View view) throws IOException {
        stopThread = true;
        outputStream.close();
        inputStream.close();
        socket.close();
        setUiEnabled(false);
        deviceConnected=false;
        stopButton.setEnabled(false);
        brakeButton.setEnabled(false);
        antitipButton.setEnabled(false);
        storageButton.setEnabled(false);
        textView.setText("\nNot connected to any data terminals.\n");
    }

    public void onClickClear(View view) {
        textView.setText("");
    }




    @Override
    public void onBackPressed() {
    }

}
