package com.example.cole.thirdattempt;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static com.example.cole.thirdattempt.Title_Screen.PREFS_NAME;

public class Codex extends AppCompatActivity {

    Button briefingButton, ReturnButton, TitleButton, Data_Lock_Button, Data_Unlock_Button;

    Button[] folders = new Button[4];
    Button[] level_changers = new Button[4];
    Button[] files = new Button[8];

    boolean all_data_unlocked = false;


    int currently_selected_folder;
    int[] access_levels = new int[4];
    int[] difficulty_levels = new int[4];

    int default_folder_color = 0xFFFFFFFF;
    int selected_folder_color = 0xFF22FFFF;


    String[] briefing_file_headers = new String[8];
    String[] briefing_file_transcripts = new String[8];

    String[][][] security_file_headers = new String[4][4][8];
    String[][][] security_file_transcripts = new String[4][4][8];

    TextView header_view, transcript_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_codex);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Shared preferences for fully unlocked data
        SharedPreferences save_file = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);


        all_data_unlocked = save_file.getBoolean("Global_Data_Unlocked", false);

        ((Warframe_Vaykor_Cipher) this.getApplication()).set_terminal_access_level(0, save_file.getInt("AEDF_access_level", 0));
        ((Warframe_Vaykor_Cipher) this.getApplication()).set_terminal_difficulty_level(0, save_file.getInt("AEDF_difficulty_level", 0));

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        briefingButton = (Button) findViewById(R.id.buttonBriefing);
        folders[0] = (Button) findViewById(R.id.buttonMoore);   // For terminal_1
        folders[1] = (Button) findViewById(R.id.buttonSloan);   // For terminal_2
        folders[2] = (Button) findViewById(R.id.buttonBridge);  // For terminal_3
        folders[3] = (Button) findViewById(R.id.buttonMudd);    // For terminal_4

        level_changers[0] = (Button) findViewById(R.id.button_AEDF_Level);   // For terminal_1
        level_changers[1] = (Button) findViewById(R.id.button_DDF_Level);   // For terminal_2
        level_changers[2] = (Button) findViewById(R.id.button_EWF_Level);  // For terminal_3
        level_changers[3] = (Button) findViewById(R.id.button_IRF_Level);    // For terminal_4

        level_changers[0].setEnabled(false);
        level_changers[1].setEnabled(false);
        level_changers[2].setEnabled(false);
        level_changers[3].setEnabled(false);
        level_changers[0].setVisibility(Button.INVISIBLE);
        level_changers[1].setVisibility(Button.INVISIBLE);
        level_changers[2].setVisibility(Button.INVISIBLE);
        level_changers[3].setVisibility(Button.INVISIBLE);

        files[0] = (Button) findViewById(R.id.button1);
        files[1] = (Button) findViewById(R.id.button2);
        files[2] = (Button) findViewById(R.id.button3);
        files[3] = (Button) findViewById(R.id.button4);
        files[4] = (Button) findViewById(R.id.button5);
        files[5] = (Button) findViewById(R.id.button6);
        files[6] = (Button) findViewById(R.id.button7);
        files[7] = (Button) findViewById(R.id.button8);

        TitleButton = (Button) findViewById(R.id.buttonTitle);
        ReturnButton = (Button) findViewById(R.id.buttonReturn);

        Data_Lock_Button = (Button) findViewById(R.id.button_lock_data);
        Data_Unlock_Button = (Button) findViewById(R.id.button_unlock_data);

        currently_selected_folder = 0; // Set to Briefing info by default

        initialize_all_text();  // Does what it says
        header_view = (TextView) findViewById(R.id.viewHeader);
        transcript_view = (TextView) findViewById(R.id.viewTranscript);


        transcript_view.setMovementMethod(new ScrollingMovementMethod());
        // Setting access levels
        for (int i = 0; i < 4; i++) {
            if (all_data_unlocked == false) { // If using the in-stack stuff
                access_levels[i] = ((Warframe_Vaykor_Cipher) this.getApplication()).get_terminal_access_level(i);
                difficulty_levels[i] = ((Warframe_Vaykor_Cipher) this.getApplication()).get_terminal_difficulty_level(i);
            }
            else { // If just using the post-stack stuff
                access_levels[i] = 8;
            }
            if (access_levels[i] <= 1)
            {
                folders[i].setEnabled(false);
            }
            folders[i].getBackground().setColorFilter(new LightingColorFilter(default_folder_color, 0xFFAA0000)); // Starting all folder buttons at white
        }

        if (all_data_unlocked == true) {
            difficulty_levels[0] = save_file.getInt("Global_AEDF_Difficulty", 1);
            difficulty_levels[1] = save_file.getInt("Global_DDF_Difficulty", 1);
            difficulty_levels[2] = save_file.getInt("Global_EWF_Difficulty", 1);
            difficulty_levels[3] = save_file.getInt("Global_IRF_Difficulty", 1);
            Data_Unlock_Button.setEnabled(false);
        }
        else {
            Data_Lock_Button.setEnabled(false);
        }
        briefingButton.getBackground().setColorFilter(new LightingColorFilter(selected_folder_color, 0xFFAA0000)); // Starting breifing button as blue
        // Starting all "files" with briefing_file_headers
        for (int i = 0; i < 8; i++) {
            files[i].setText(briefing_file_headers[i]);
        }
    }


    public void unlockAlldata(View view) {
        all_data_unlocked = true;
        Data_Unlock_Button.setEnabled(false);
        Data_Lock_Button.setEnabled(true);


        SharedPreferences save_file = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor savior = save_file.edit();

        savior.putBoolean("Global_Data_Unlocked", all_data_unlocked);
        for (int i = 0; i < 4; i++) {
            access_levels[i] = 9;

            if (access_levels[i] <= 1)
            {
                folders[i].setEnabled(false);
            }
            else {
                folders[i].setEnabled(true);
            }
        }
        difficulty_levels[0] = save_file.getInt("Global_AEDF_Difficulty", 1);
        difficulty_levels[1] = save_file.getInt("Global_DDF_Difficulty", 1);
        difficulty_levels[2] = save_file.getInt("Global_EWF_Difficulty", 1);
        difficulty_levels[3] = save_file.getInt("Global_IRF_Difficulty", 1);

        // Can't forget to actually save the changes XD
        savior.apply();

        if (currently_selected_folder == 0) {
            selectBriefing(view);
        }
        else if (currently_selected_folder == 1) {
            selectAEDF(view);
        }
        else if (currently_selected_folder == 2) {
            selectDDF(view);
        }
        else if (currently_selected_folder == 3) {
            selectEWF(view);
        }
        else {// if (currently_selected_folder == 4) {
            selectIRF(view);
        }
    }

    public void LockAlldata(View view) {
        all_data_unlocked = false;
        Data_Unlock_Button.setEnabled(true);
        Data_Lock_Button.setEnabled(false);

        SharedPreferences save_file = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor savior = save_file.edit();

        savior.putBoolean("Global_Data_Unlocked", all_data_unlocked);
        for (int i = 0; i < 4; i++) {
            access_levels[i] = ((Warframe_Vaykor_Cipher) this.getApplication()).get_terminal_access_level(i);
            difficulty_levels[i] = ((Warframe_Vaykor_Cipher) this.getApplication()).get_terminal_difficulty_level(i);

            if (access_levels[i] <= 1)
            {
                folders[i].setEnabled(false);
            }
            else {
                folders[i].setEnabled(true);
            }
        }
        // Can't forget to actually save the changes XD
        savior.apply();

        if (currently_selected_folder == 0) {
            selectBriefing(view);
        }
        else if (currently_selected_folder == 1) {
            selectAEDF(view);
        }
        else if (currently_selected_folder == 2) {
            selectDDF(view);
        }
        else if (currently_selected_folder == 3) {
            selectEWF(view);
        }
        else {// if (currently_selected_folder == 4) {
            selectIRF(view);
        }
    }



    public void update_difficulty(View view) {
        if (all_data_unlocked == true) {
            difficulty_levels[currently_selected_folder - 1]++;
            if (difficulty_levels[currently_selected_folder - 1] > 4) {
                difficulty_levels[currently_selected_folder - 1] = 1;
            }
            level_changers[currently_selected_folder - 1].setText(String.valueOf(difficulty_levels[currently_selected_folder - 1]));


            SharedPreferences save_file = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
            SharedPreferences.Editor savior = save_file.edit();

            if (currently_selected_folder == 1) {
                savior.putInt("Global_AEDF_Difficulty", 1);
                selectAEDF(view);
            } else if (currently_selected_folder == 2) {
                save_file.getInt("Global_DDF_Difficulty", 1);
                selectDDF(view);
            } else if (currently_selected_folder == 3) {
                save_file.getInt("Global_EWF_Difficulty", 1);
                selectEWF(view);
            } else {
                save_file.getInt("Global_IRF_Difficulty", 1);
                selectIRF(view);
            }

            // Can't forget to actually save the changes XD
            savior.apply();
        }
    }

    public void selectBriefing(View view) { // The briefing files
        currently_selected_folder = 0;
        for (int i = 0; i < 8; i++) {
            files[i].setEnabled(true);
            files[i].setVisibility(Button.VISIBLE);
            files[i].setText(briefing_file_headers[i]);
        }

        briefingButton.getBackground().setColorFilter(new LightingColorFilter(selected_folder_color, 0xFFAA0000));
        folders[0].getBackground().setColorFilter(new LightingColorFilter(default_folder_color, 0xFFAA0000));
        folders[1].getBackground().setColorFilter(new LightingColorFilter(default_folder_color, 0xFFAA0000));
        folders[2].getBackground().setColorFilter(new LightingColorFilter(default_folder_color, 0xFFAA0000));
        folders[3].getBackground().setColorFilter(new LightingColorFilter(default_folder_color, 0xFFAA0000));

        level_changers[0].setEnabled(false);
        level_changers[1].setEnabled(false);
        level_changers[2].setEnabled(false);
        level_changers[3].setEnabled(false);
        level_changers[0].setVisibility(Button.INVISIBLE);
        level_changers[1].setVisibility(Button.INVISIBLE);
        level_changers[2].setVisibility(Button.INVISIBLE);
        level_changers[3].setVisibility(Button.INVISIBLE);
    }

    public void selectAEDF(View view) { // The first terminal's files
        currently_selected_folder = 1;
        for (int i = 0; i < access_levels[0] - 1; i++) {
            files[i].setEnabled(true);
            files[i].setVisibility(Button.VISIBLE);
            files[i].setText(security_file_headers[0][difficulty_levels[0] - 1][i]);
        }
        for (int i = 0; i < 9 - access_levels[0]; i++) {
            files[i + access_levels[0] - 1].setEnabled(true);
            files[i + access_levels[0] - 1].setVisibility(Button.INVISIBLE);
        }
        briefingButton.getBackground().setColorFilter(new LightingColorFilter(default_folder_color, 0xFFAA0000));
        folders[0].getBackground().setColorFilter(new LightingColorFilter(selected_folder_color, 0xFFAA0000));
        folders[1].getBackground().setColorFilter(new LightingColorFilter(default_folder_color, 0xFFAA0000));
        folders[2].getBackground().setColorFilter(new LightingColorFilter(default_folder_color, 0xFFAA0000));
        folders[3].getBackground().setColorFilter(new LightingColorFilter(default_folder_color, 0xFFAA0000));

        level_changers[0].setEnabled(true);
        level_changers[1].setEnabled(false);
        level_changers[2].setEnabled(false);
        level_changers[3].setEnabled(false);
        level_changers[0].setVisibility(Button.VISIBLE);
        level_changers[1].setVisibility(Button.INVISIBLE);
        level_changers[2].setVisibility(Button.INVISIBLE);
        level_changers[3].setVisibility(Button.INVISIBLE);
    }

    public void selectDDF(View view) { // The second terminal's files
        currently_selected_folder = 2;
        for (int i = 0; i < access_levels[1] - 1; i++) {
            files[i].setEnabled(true);
            files[i].setVisibility(Button.VISIBLE);
            files[i].setText(security_file_headers[1][difficulty_levels[1] - 1][i]);
        }
        for (int i = 0; i < 9 - access_levels[1]; i++) {
            files[i + access_levels[1] - 1].setEnabled(true);
            files[i + access_levels[1] - 1].setVisibility(Button.INVISIBLE);
        }
        briefingButton.getBackground().setColorFilter(new LightingColorFilter(default_folder_color, 0xFFAA0000));
        folders[0].getBackground().setColorFilter(new LightingColorFilter(default_folder_color, 0xFFAA0000));
        folders[1].getBackground().setColorFilter(new LightingColorFilter(selected_folder_color, 0xFFAA0000));
        folders[2].getBackground().setColorFilter(new LightingColorFilter(default_folder_color, 0xFFAA0000));
        folders[3].getBackground().setColorFilter(new LightingColorFilter(default_folder_color, 0xFFAA0000));

        level_changers[0].setEnabled(false);
        level_changers[1].setEnabled(true);
        level_changers[2].setEnabled(false);
        level_changers[3].setEnabled(false);
        level_changers[0].setVisibility(Button.INVISIBLE);
        level_changers[1].setVisibility(Button.VISIBLE);
        level_changers[2].setVisibility(Button.INVISIBLE);
        level_changers[3].setVisibility(Button.INVISIBLE);
    }

    public void selectEWF(View view) { // The third terminal's files
        currently_selected_folder = 3;
        for (int i = 0; i < access_levels[2] - 1; i++) {
            files[i].setEnabled(true);
            files[i].setVisibility(Button.VISIBLE);
            files[i].setText(security_file_headers[2][difficulty_levels[2] - 1][i]);
        }
        for (int i = 0; i < 9 - access_levels[2]; i++) {
            files[i + access_levels[2] - 1].setEnabled(true);
            files[i + access_levels[2] - 1].setVisibility(Button.INVISIBLE);
        }
        briefingButton.getBackground().setColorFilter(new LightingColorFilter(default_folder_color, 0xFFAA0000));
        folders[0].getBackground().setColorFilter(new LightingColorFilter(default_folder_color, 0xFFAA0000));
        folders[1].getBackground().setColorFilter(new LightingColorFilter(default_folder_color, 0xFFAA0000));
        folders[2].getBackground().setColorFilter(new LightingColorFilter(selected_folder_color, 0xFFAA0000));
        folders[3].getBackground().setColorFilter(new LightingColorFilter(default_folder_color, 0xFFAA0000));

        level_changers[0].setEnabled(false);
        level_changers[1].setEnabled(false);
        level_changers[2].setEnabled(true);
        level_changers[3].setEnabled(false);
        level_changers[0].setVisibility(Button.INVISIBLE);
        level_changers[1].setVisibility(Button.INVISIBLE);
        level_changers[2].setVisibility(Button.VISIBLE);
        level_changers[3].setVisibility(Button.INVISIBLE);
    }

    public void selectIRF(View view) { // The fourth terminal's files
        currently_selected_folder = 4;
        for (int i = 0; i < access_levels[3] - 1; i++) {
            files[i].setEnabled(true);
            files[i].setVisibility(Button.VISIBLE);
            files[i].setText(security_file_headers[3][difficulty_levels[3] - 1][i]);
        }
        for (int i = 0; i < 9 - access_levels[3]; i++) {
            files[i + access_levels[3] - 1].setEnabled(true);
            files[i + access_levels[3] - 1].setVisibility(Button.INVISIBLE);
        }
        briefingButton.getBackground().setColorFilter(new LightingColorFilter(default_folder_color, 0xFFAA0000));
        folders[0].getBackground().setColorFilter(new LightingColorFilter(default_folder_color, 0xFFAA0000));
        folders[1].getBackground().setColorFilter(new LightingColorFilter(default_folder_color, 0xFFAA0000));
        folders[2].getBackground().setColorFilter(new LightingColorFilter(default_folder_color, 0xFFAA0000));
        folders[3].getBackground().setColorFilter(new LightingColorFilter(selected_folder_color, 0xFFAA0000));

        level_changers[0].setEnabled(false);
        level_changers[1].setEnabled(false);
        level_changers[2].setEnabled(false);
        level_changers[3].setEnabled(true);
        level_changers[0].setVisibility(Button.INVISIBLE);
        level_changers[1].setVisibility(Button.INVISIBLE);
        level_changers[2].setVisibility(Button.INVISIBLE);
        level_changers[3].setVisibility(Button.VISIBLE);
    }

    public void disableDirectory() {
        for (int i = 0; i < 8; i++) {
            files[i].setEnabled(false);
            files[i].setVisibility(Button.INVISIBLE);
        }
        briefingButton.setEnabled(false);
        briefingButton.setVisibility(Button.INVISIBLE);
        for (int i = 0; i < 4; i++) {
            folders[i].setEnabled(false);
            folders[i].setVisibility(Button.INVISIBLE);
        }
        TitleButton.setEnabled(false);
        TitleButton.setVisibility(Button.INVISIBLE);
        ReturnButton.setEnabled(true);
        ReturnButton.setVisibility(Button.VISIBLE);
        header_view.setVisibility(TextView.VISIBLE);
        transcript_view.setVisibility(TextView.VISIBLE);


        level_changers[0].setEnabled(false);
        level_changers[1].setEnabled(false);
        level_changers[2].setEnabled(false);
        level_changers[3].setEnabled(false);
        level_changers[0].setVisibility(Button.INVISIBLE);
        level_changers[1].setVisibility(Button.INVISIBLE);
        level_changers[2].setVisibility(Button.INVISIBLE);
        level_changers[3].setVisibility(Button.INVISIBLE);


        Data_Unlock_Button.setEnabled(false);
        Data_Lock_Button.setEnabled(false);

        Data_Unlock_Button.setVisibility(Button.INVISIBLE);
        Data_Lock_Button.setVisibility(Button.INVISIBLE);
    }

    public void enableDirectory(View view) {
        if (currently_selected_folder == 0) {
            selectBriefing(view);
        }
        else if (currently_selected_folder == 1) {
            selectAEDF(view);
        }
        else if (currently_selected_folder == 2) {
            selectDDF(view);
        }
        else if (currently_selected_folder == 3) {
            selectEWF(view);
        }
        else {
            selectIRF(view);
        }
        briefingButton.setEnabled(true);
        briefingButton.setVisibility(Button.VISIBLE);
        for (int i = 0; i < 4; i++) {
            folders[i].setVisibility(Button.VISIBLE);
            if (access_levels[i] > 1)
            {
                folders[i].setEnabled(true);
            }
        }
        TitleButton.setEnabled(true);
        TitleButton.setVisibility(Button.VISIBLE);
        ReturnButton.setEnabled(false);
        ReturnButton.setVisibility(Button.INVISIBLE);
        header_view.setVisibility(TextView.INVISIBLE);
        transcript_view.setVisibility(TextView.INVISIBLE);

        if (currently_selected_folder == 0) {
            selectBriefing(view);
        }
        else if (currently_selected_folder == 1) {
            selectAEDF(view);
        }
        else if (currently_selected_folder == 2) {
            selectDDF(view);
        }
        else if (currently_selected_folder == 3) {
            selectEWF(view);
        }
        else {
            selectIRF(view);
        }
        if (all_data_unlocked == true) {
            Data_Lock_Button.setEnabled(true);
        }
        else {
            Data_Unlock_Button.setEnabled(true);
        }

        Data_Unlock_Button.setVisibility(Button.VISIBLE);
        Data_Lock_Button.setVisibility(Button.VISIBLE);
    }


    public void go_to_Title(View view) {
        Intent intent = new Intent(this, Title_Screen.class);
        startActivity(intent);
    }

    // All 8 functions for the "file" buttons

    public void openFile_1(View view) {
        if (currently_selected_folder != 0) {
            header_view.setText(security_file_headers[currently_selected_folder - 1][difficulty_levels[currently_selected_folder - 1] - 1][0]);
            transcript_view.setText(security_file_transcripts[currently_selected_folder - 1][difficulty_levels[currently_selected_folder - 1] - 1][0]);
        }
        else {
            header_view.setText(briefing_file_headers[0]);
            transcript_view.setText(briefing_file_transcripts[0]);
        }
        disableDirectory();
    }

    public void openFile_2(View view) {
        disableDirectory();
        if (currently_selected_folder != 0) {
            header_view.setText(security_file_headers[currently_selected_folder - 1][difficulty_levels[currently_selected_folder - 1] - 1][1]);
            transcript_view.setText(security_file_transcripts[currently_selected_folder - 1][difficulty_levels[currently_selected_folder - 1] - 1][1]);
        }
        else {
            header_view.setText(briefing_file_headers[1]);
            transcript_view.setText(briefing_file_transcripts[1]);
        }
        header_view.setVisibility(TextView.VISIBLE);
        transcript_view.setVisibility(TextView.VISIBLE);
    }
    public void openFile_3(View view) {
        disableDirectory();
        if (currently_selected_folder != 0) {
            header_view.setText(security_file_headers[currently_selected_folder - 1][difficulty_levels[currently_selected_folder - 1] - 1][2]);
            transcript_view.setText(security_file_transcripts[currently_selected_folder - 1][difficulty_levels[currently_selected_folder - 1] - 1][2]);
        }
        else {
            header_view.setText(briefing_file_headers[2]);
            transcript_view.setText(briefing_file_transcripts[2]);
        }
        header_view.setVisibility(TextView.VISIBLE);
        transcript_view.setVisibility(TextView.VISIBLE);
    }
    public void openFile_4(View view) {
        disableDirectory();
        if (currently_selected_folder != 0) {
            header_view.setText(security_file_headers[currently_selected_folder - 1][difficulty_levels[currently_selected_folder - 1] - 1][3]);
            transcript_view.setText(security_file_transcripts[currently_selected_folder - 1][difficulty_levels[currently_selected_folder - 1] - 1][3]);
        }
        else {
            header_view.setText(briefing_file_headers[3]);
            transcript_view.setText(briefing_file_transcripts[3]);
        }
        header_view.setVisibility(TextView.VISIBLE);
        transcript_view.setVisibility(TextView.VISIBLE);
    }
    public void openFile_5(View view) {
        disableDirectory();
        if (currently_selected_folder != 0) {
            header_view.setText(security_file_headers[currently_selected_folder - 1][difficulty_levels[currently_selected_folder - 1] - 1][4]);
            transcript_view.setText(security_file_transcripts[currently_selected_folder - 1][difficulty_levels[currently_selected_folder - 1] - 1][4]);
        }
        else {
            header_view.setText(briefing_file_headers[4]);
            transcript_view.setText(briefing_file_transcripts[4]);
        }
        header_view.setVisibility(TextView.VISIBLE);
        transcript_view.setVisibility(TextView.VISIBLE);
    }
    public void openFile_6(View view) {
        disableDirectory();
        if (currently_selected_folder != 0) {
            header_view.setText(security_file_headers[currently_selected_folder - 1][difficulty_levels[currently_selected_folder - 1] - 1][5]);
            transcript_view.setText(security_file_transcripts[currently_selected_folder - 1][difficulty_levels[currently_selected_folder - 1] - 1][5]);
        }
        else {
            header_view.setText(briefing_file_headers[5]);
            transcript_view.setText(briefing_file_transcripts[5]);
        }
        header_view.setVisibility(TextView.VISIBLE);
        transcript_view.setVisibility(TextView.VISIBLE);
    }
    public void openFile_7(View view) {
        disableDirectory();
        if (currently_selected_folder != 0) {
            header_view.setText(security_file_headers[currently_selected_folder - 1][difficulty_levels[currently_selected_folder - 1] - 1][6]);
            transcript_view.setText(security_file_transcripts[currently_selected_folder - 1][difficulty_levels[currently_selected_folder - 1] - 1][6]);
        }
        else {
            header_view.setText(briefing_file_headers[6]);
            transcript_view.setText(briefing_file_transcripts[6]);
        }
        header_view.setVisibility(TextView.VISIBLE);
        transcript_view.setVisibility(TextView.VISIBLE);
    }
    public void openFile_8(View view) {
        disableDirectory();
        if (currently_selected_folder != 0) {
            header_view.setText(security_file_headers[currently_selected_folder - 1][difficulty_levels[currently_selected_folder - 1] - 1][7]);
            transcript_view.setText(security_file_transcripts[currently_selected_folder - 1][difficulty_levels[currently_selected_folder - 1] - 1][7]);
        }
        else {
            header_view.setText(briefing_file_headers[7]);
            transcript_view.setText(briefing_file_transcripts[7]);
        }
        header_view.setVisibility(TextView.VISIBLE);
        transcript_view.setVisibility(TextView.VISIBLE);
    }


    public void initialize_all_text () { // Sets up all text headers and transcripts

        // Start with briefing files
        briefing_file_headers[0] = "Wake Up, Tenno!";
        briefing_file_transcripts[0] = "Attention Tenno! \n\n" +
                "It is time to awaken from your slumber. Welcome to a new century. Welcome to the Steel Meridian. \n\n" +
                "These are trying times and adverse circumstances have prevented me from greeting you in person. " +
                "Such a meeting will have to wait. " +
                "Four of our organization's foremost veterans, and my trusted advisors, will guide you in my stead. " +
                "These are your Mentors. " +
                "Follow their lead and heed their advice. \n\n" +
                "You are the future of the Steel Meridian. You will determine its destiny.\n\n" +
                "-General Cressa Tal";
        briefing_file_headers[1] = "Situation";
        briefing_file_transcripts[1] = "Tenno: The Steel Meridian has crumbled. Our members are scattered and cannot remember what once was. Our equipment has been ransacked. Our Warframes are lost. The Red Veil, our former allies, have turned their backs on us in our time of need. My attempts to reconcile with our former partners have proved futile. But you have each other, and that is what counts. \n\n- General Cressa Tal";
        briefing_file_headers[2] = "Equipment";
        briefing_file_transcripts[2] = "Tenno: An unknown source has nullified all Tenno powers in the vicinity of Cortech. In addition, we are missing our caches of Warframes themselves. Accordingly, you must accomplish your mission with the resources you have. Be warned: any hit from enemies, even on hair or clothes, will put you in critical condition. I order you to play dead should such a thing occur; enemies will not attack a motionless, fallen Tenno. Should you continue to struggle, however, they may end you permanently. Though it may be of minimal assistance, my intelligence indicates a cache of weapons at the Corpus outside of the AEDF. Liberate what you can. Make me proud. \n\n- General Cressa Tal";
        briefing_file_headers[3] = "Mission";
        briefing_file_transcripts[3] = "Tenno: As newly awoken Tenno, you are free from the mental blocks that have plagued us all. While  I endeavor to convince the Red Veil to aid us once more, I am relying on you to investigate recent events to the fullest. I will send you my location once I make it to the Red Veil’s leader. Be on the alert for my hidden messages, decipher their new command center’s location, and join me when your investigation is complete. I must warn you that not all is as it seems on this campus. Walking openly into the arms of danger is foolish. Be as mysterious as the dark side of the moon.  \n\n- General Cressa Tal";
        briefing_file_headers[4] = "Vaults";
        briefing_file_transcripts[4] = "Tenno: I suspect that Cortech’s High Security Data Vaults contain information vital to uncovering the cause of the recent disturbing events. The exact locations of these vaults are known to your mentors. For now, it is sufficient to say that they are located in the general vicinity of the Advanced Electronic Design Facility (AEDF), Energy Weapon Facility (EWF), Deployment Design Facility (DDF), and Infestation Research Facility (IRF). You may remember these as “Moore”, “Bridge”, “Sloan”, and “North Mudd”. Intelligence suggests that these High Security Data Vaults are laden with traps. All such vaults are saturated with hard radiation for five minutes at 45 minute intervals. Such radiation will instantly kill you in your unprotected state. Your mentors know the vaults’ purging schedules: do not tarry overlong in each vault. Recover as much information as you can. Save us with the strength of a raging fire. \n\n-General Cressa Tal";
        briefing_file_headers[5] = "Guards";
        briefing_file_transcripts[5] = "Tenno: Corpus guards patrol all Data Vaults. Most that you will encounter are Crewmen. These guards are preoccupied with their own lives, compensated very little for a very dangerous job. Nevertheless, deal with them as you see fit. I will pass along a few words of caution with regards to the Crewmen: First, all Corpus guards are, by default, heavily armored, with only two weak points. Ranged strikes to the helmet will temporarily disorient them, but melee attacks to the helmet are completely ineffective. Due to the air’s toxicity levels, they also have a volatile air supply attached to their back. Detaching the air supply will cause them to choke out into unconsciousness and slowly die over 5 minutes; rupturing their air supply while attached will instantly kill them. Second, be wary of the Corpus guards. In fact, be wary of all enemies you may encounter. I cannot foresee all scenarios, and some enemies may have entirely different weak points and play by entirely different rules. The only thing that is certain is that they are all lethal to you. Move with the force of a great typhoon. \n\n- General Cressa Tal";
        briefing_file_headers[6] = "Hacking";
        briefing_file_transcripts[6] = "Tenno: Your legacy devices’ Bluetooth will allow you to hack into the data terminals. My intelligence has reported that the Corpus default passcodes are “1234.” After pairing your devices to the data terminal, “Connect” using the Terminal Access window. Once connected, start hacking. Stay close to the terminal. If you venture away too soon, then the download will fail. Triggering too many virtual alerts will set off a real one. Wait for the alerts to decay if necessary. Be swift as the coursing river. \n\n- General Cressa Tal";
        briefing_file_headers[7] = "Reward";
        briefing_file_transcripts[7] = "Tenno: Strength. Honor. Sacrifice. That is what it means to be Tenno of the Steel Meridian. Yet greed pulls at the heart of the noblest among you. I understand. Reap the rewards of your labor by breaking open the resource containers scattered among the vaults. Enjoy your prizes. You will have earned them. \n\n- General Cressa Tal";

        // Moving on to the 128 security logs

        // Start with the AEDF logs
        // Difficulty: 1
        security_file_headers[0][0][0] = "AEDF Security Log 08:55 05/26/0017";
        security_file_transcripts[0][0][0] = "Oxygen release should under no circumstances be an element of normal operation of the batteries! Either your instruments are broken, or you have a leak in the lab. Either way, get that fixed before something explodes. \n\nAuthor: Crewman Gamma {ERROR: Message originates from ‘Cressa Tal’, not gammaman@gmail.com. Contact the IT department for questions about messaging system errors.}";
        security_file_headers[0][0][1] = "AEDF Security Log 04:20 01/01/0011";
        security_file_transcripts[0][0][1] = "Re: High-Pitched Noises \n\nFound three unauthorized intruders in one of the labs 5 minutes ago. Showed them the door 5 minutes ago. Specifically, the closed elevator door. If you could send custodial services down here soon, that’d be great. \n\nAuthor: Crewman Gamma";
        security_file_headers[0][0][2] = "AEDF Security Log 02:05 06/12/0012";
        security_file_transcripts[0][0][2] = "Just survived an encounter with a Warframe. Type: Nova class. Weaponry: some kind of ornate stick. Not sure what it was after. Going to go find a medic now. \n\nAuthor: Crewman Gamma";
        security_file_headers[0][0][3] = "AEDF Security Log 12:30 09/15/0015";
        security_file_transcripts[0][0][3] = "Re: Pay for the damages \n\nBuddy, I’m not the one that left a bug in their code that caused a tarnation JACKAL to go haywire while one of the VIPs was here. You can either deal with the damage to your robot, or pay for your mistake with your life if I decide to report your incompetence: your choice. \n\nAuthor: Crewman Gamma";
        security_file_headers[0][0][4] = "AEDF Security Broadcast 04:15 01/01/011";
        security_file_transcripts[0][0][4] = "I am the law. Drop your weapons. You are all under arrest. This is your final warning. \n\nAuthor: Crewman Gamma";
        security_file_headers[0][0][5] = "AEDF Security Log 15:34 09/16/0015";
        security_file_transcripts[0][0][5] = "Re: Pay for the damages \n\nActually, I have a request for you. I’d like weapons to replace the ones I broke saving the VIP from your monstrosity. Proper weapons, none of this cheapo generic stun-stick stuff. I want a Prova, like the ones we had back in the front lines. \n\nAuthor: Crewman Gamma";
        security_file_headers[0][0][6] = "AEDF Security Log 15:51 08/29/0010";
        security_file_transcripts[0][0][6] = "Re: Slight Genetic Modification \n\nYou did WHAT to me? I don’t care if that was hidden somewhere in the fine print of my contract, the way you did that was not acceptable. \n\nAuthor: Crewman Gamma";
        security_file_headers[0][0][7] = "AEDF Security Log 10:59 05/25/17";
        security_file_transcripts[0][0][7] = "Re: Sargus Ruk \n\nI’m not worried about him. I’m worried about his lieutenant. I fought that guy maybe a decade ago on Mars, and he was strong then. Given the Grineer’s attitude towards augmentation, he’s probably even stronger now. \n\nAuthor: Crewman Gamma";

        // Difficulty: 2
        security_file_headers[0][1][0] = "AEDF Security Log 09:40 05/26/0017";
        security_file_transcripts[0][1][0] = "Re: No money for a raise \n\nBetter watch your back. Soldier. \n\nAuthor: Crewman Gamma {ERROR: Message originates from ‘Cressa Tal’, not gammaman@gmail.com. Contact the IT department for questions about messaging system errors.}";
        security_file_headers[0][1][1] = "AEDF Security Log 12:32 09/15/0015";
        security_file_transcripts[0][1][1] = "Re: Pay for the damages \n\nYou’re telling me NOW that the thing activates as soon as it detects even the slightest sound? You built a 2-story dog-like robot with rockets and machine guns, intentionally programmed it to enter combat mode at 60 dB, and DIDN’T TELL ANYONE?! \n\nAuthor: Crewman Gamma";
        security_file_headers[0][1][2] = "AEDF Security Log 02:05 07/30/0012";
        security_file_transcripts[0][1][2] = "Just survived another encounter with a Warframe. Type: Mesa class. Weaponry: guns that are literally part of its arms. This one was trying to get into the JACKAL lab, probably to sabotage the research being done there. I took surprisingly little damage from its attacks; looks like the new body armor is actually quite decent against small-arms fire. \n\nAuthor: Crewman Gamma";
        security_file_headers[0][1][3] = "AEDF Security Log 03:10 04/11/0014";
        security_file_transcripts[0][1][3] = "Bored, so might as well record useful info for whoever eventually replaces me. Datum #1: This job is really dull if you’re good at it, because enemies stop trying to infiltrate. \n\nAuthor: Crewman Gamma";
        security_file_headers[0][1][4] = "AEDF Security Log 01:11 06/28/0016";
        security_file_transcripts[0][1][4] = "Re: Explosion \n\nFound another Grineer infiltrator this morning. Tried to talk their way out of the situation; something something saving the world nonsense. Seriously, like I’d fall for that. \n\nAuthor: Crewman Gamma";
        security_file_headers[0][1][5] = "AEDF Security Log 01:13 06/28/0016";
        security_file_transcripts[0][1][5] = "Re: Explosion \n\nAlmost forgot, can I have a couple bodybags? The researchers down here don’t seem to appreciate my efforts on their behalf. \n\nAuthor: Crewman Gamma";
        security_file_headers[0][1][6] = "AEDF Security Log 11:13 05/25/0017";
        security_file_transcripts[0][1][6] = "Re: Sargus Ruk \n\nLech Kril is not someone you guys can fight. He has no weak points to shoot; it requires a melee weapon to take out his energy source. Besides, he has a more powerful machine gun than either of you, AND a hammer. \n\nAuthor: Crewman Gamma";
        security_file_headers[0][1][7] = "AEDF Security Log 15:58 08/29/0010";
        security_file_transcripts[0][1][7] = "Re: Slight Genetic Modification \n\nOk, that makes a lot more sense. I’ll stop yelling. Adaptation to this atmosphere seems really useful, actually, and will probably increase my survivability a lot. Still am not happy with you literally drugging my coffee so that you could perform the modification though. Just ASK next time, ok? \n\nAuthor: Crewman Gamma";

        // Difficulty: 3
        security_file_headers[0][2][0] = "AEDF Security Log 10:29 05/26/0017";
        security_file_transcripts[0][2][0] = "Re: Just in Case \n\nOnly Alad V knows how to reanimate a dead Warframe. Look around you: this isn’t Jupiter. Everyone knows the man doesn’t leave his planet, which means that we have to send any of those there. \n\nAuthor: Crewman Gamma {ERROR: Message originates from ‘Cressa Tal’, not gammaman@gmail.com. Contact the IT department for questions about messaging system errors.}";
        security_file_headers[0][2][1] = "AEDF Security Log 03:10 04/11/0014";
        security_file_transcripts[0][2][1] = "Datum #2: If bringing VIPs on a tour, tape their mouths shut so that they don’t set off the stupidly programmed kill-bots down here. \n\nAuthor: Crewman Gamma";
        security_file_headers[0][2][2] = "AEDF Security Log 15:34 09/16/0015";
        security_file_transcripts[0][2][2] = "Re: Pay for the damages \n\nAnd by Prova, I mean Provas. Two of them. With spare batteries. \n\nAuthor: Crewman Gamma";
        security_file_headers[0][2][3] = "AEDF Security Log 11:22 05/25/17";
        security_file_transcripts[0][2][3] = "Re: Sargus Ruk \n\nSo what you’re saying is that we don’t actually know if Kril is showing up? Because if he does show up, he’s coming here for vengeance. It’s a long story, but I didn’t actually fight him with weapons. We were both in a food-eating competition hosted during one of the truces. It...didn’t end well. \n\nAuthor: Crewman Gamma";
        security_file_headers[0][2][4] = "AEDF Security Log 11:24 05/25/17";
        security_file_transcripts[0][2][4] = "Re: Sargus Ruk \n\nTo use more ancient references: if Sargus is the Iron Man of the Grineer, Kril is the Hulk. Whose secret is that he’s always hungry. \n\nAuthor: Crewman Gamma";
        security_file_headers[0][2][5] = "AEDF Security Log 19:30 02/18/0013";
        security_file_transcripts[0][2][5] = "Almost died to a Limbo-class Warframe 10 minutes ago. The thing looked so much like a high-class visitor that I almost didn’t register the rapier it was carrying. Don’t actually know what it was doing here, it just seemed to be looking in the lab windows. \n\nAuthor: Crewman Gamma";
        security_file_headers[0][2][6] = "AEDF Security Log 14:37 01/31/0016";
        security_file_transcripts[0][2][6] = "Had a researcher trip while walking down the hallway and get instantly destroyed by another lab’s new OSPREY. Has anyone considered NOT having the robots be so incredibly sensitive to sound WHILE UNDER ACTIVE DEVELOPMENT? I had to fill out so much paperwork today because of it. \n\nAuthor: Crewman Gamma";
        security_file_headers[0][2][7] = "AEDF Security Broadcast";
        security_file_transcripts[0][2][7] = "07:01 05/26/17 Kril, we don’t have to do this. Drop the researcher and face me like a man. I brought burritos. \n\nAuthor: Crewman Gamma";

        // Difficulty: 4
        security_file_headers[0][3][0] = "AEDF Security Log 11:10 05/26/0017";
        security_file_transcripts[0][3][0] = "Re: OSPREY PDR documents \n\nTellurium can be an element of the new OSPREY if necessary, but we would appreciate you finding an alternative... like, say, oxium. Tellurium’s not exactly common or inexpensive. \n\nAuthor: Crewman Gamma {ERROR: Message originates from ‘Cressa Tal’, not gammaman@gmail.com. Contact the IT department for questions about messaging system errors.}";
        security_file_headers[0][3][1] = "AEDF Security Log 00:14 03/14/0016";
        security_file_transcripts[0][3][1] = "Found something on the corpse of the latest infiltrator. Looks to be a pre-Collapse ID card. Can only make out some of the letters on it, the rest are too smudged to read: G - l -e -something -something - e - o -r - something - e. Don’t know why they wanted it, never heard of that person. \n\nAuthor: Crewman Gamma";
        security_file_headers[0][3][2] = "AEDF Security Broadcast 07:03 05/26/17";
        security_file_transcripts[0][3][2] = "Would you kindly holster the Gorgon. Would you kindly stop pointing it at me. Would you kindly - y’know what, this isn’t working. Good thing I charged my Provas. \n\nAuthor: Crewman Gamma";
        security_file_headers[0][3][3] = "AEDF Security Log 06:55 05/26/17";
        security_file_transcripts[0][3][3] = "Re: Sargus Ruk \n\nTold you he’d show up. Also, if Sargus hasn’t blown anything up yet, he’s probably about to. Kril literally dropped in… through four floors. \n\nAuthor: Crewman Gamma";
        security_file_headers[0][3][4] = "AEDF Security Log 08:40 05/26/0017";
        security_file_transcripts[0][3][4] = "Re: Replacement Trainee \n\nLooks like it’s time for me to retire. The first thing you should probably do with my replacement is have them complete Physical Regimen #3 over at the CRTF";
        security_file_headers[0][3][5] = "AEDF Security Log 03:36 11/02/0015";
        security_file_transcripts[0][3][5] = "Re: Slight Genetic Modification \n\nJust woke up after apparently getting my air supply cut. Normally that kills people; I’m happy to report that your genetic modification maaaaay have saved my life. You’re still staying in jail, though. \n\nAuthor: Crewman Gamma";
        security_file_headers[0][3][6] = "AEDF Security Log 03:40 11/02/0015";
        security_file_transcripts[0][3][6] = "I regret to report that a single Warframe made it past into one of the labs in today’s incident. I have no visual confirmation of what class it was, but based on the fact that I have complete amnesia of 40 minutes of time today, it was almost certainly a Nyx class. Based on the slashes in the more complete Jackal, it was here on a sabotage mission and used some type of bladed weapon. \n\nAuthor: Crewman Gamma";
        security_file_headers[0][3][7] = "AEDF Security Log 08:43 05/26/0017";
        security_file_transcripts[0][3][7] = "Re: Replacement Trainee \n\nFresh meat, eh? I’m sorry for you. Anyway: CRTF is the Cortech Renovated Training Facility, as compared to the Cortech Ancient Training Facility. I’ve heard their pre-Collapse names were hilariously confusing. If it makes it any easier, you literally can’t get into the wrong one. \n\nAuthor: Crewman Gamma";

        // Move on to the DDF logs
        // Difficulty: 1
        security_file_headers[1][0][0] = "DDF Security Log 08:55 05/26/0017";
        security_file_transcripts[1][0][0] = "Re: Possible Solution \n\nCarbon sounds great as an element of our shuttles’ improved combat armor. Keep up the good work! \n\nAuthor: Crewman Kappa {ERROR: Message originates from ‘Cressa Tal’, not kappaman@gmail.com. Contact the IT department for questions about messaging system errors.}";
        security_file_headers[1][0][1] = "DDF Security Log 10:13 01/01/0001";
        security_file_transcripts[1][0][1] = "Re: Accommodate the good doctor \n\nAccommodate him, I will, because I have to. But “good” doctor? He’s a filthy Grineer! And not just any Grineer, either. \n\nAuthor: Crewman Kappa";
        security_file_headers[1][0][2] = "DDF Security Log 14:20 03/01/0001";
        security_file_transcripts[1][0][2] = "I would appreciate it if your “Jet Propelled Launch Center” would give me an hour of their time. There is an artifact, lost near the end of the Collapse, that needs to be found. \n\nAuthor: Dr. Tyl Regor";
        security_file_headers[1][0][3] = "DDF Security Log 23:14 10/12/0015";
        security_file_transcripts[1][0][3] = "Re: Information on Cressa Tal \n\nWhy contact me now about this? I would have thought you would know even more than I about her. After all, you’ve been working together for quite a while… \n\nAuthor: Dr. Tyl Regor";
        security_file_headers[1][0][4] = "DDF Security Log 12:15 01/01/0001";
        security_file_transcripts[1][0][4] = "Re: Accommodate the good doctor \n\nHe ran their most advanced augmentation program during the War! This thing is the whole reason we lost Uranus in the first place! Do you remember how many soldiers we lost fighting his creations? \n\nAuthor: Crewman Kappa";
        security_file_headers[1][0][5] = "DDF Security Log 19:46 03/22/0003";
        security_file_transcripts[1][0][5] = "A note concerning the “good” doctor. I believe he came here to do augmentation research. So, why does he have a theoretical physics lab in this building? We do spaceflight operations here, not… portals or whatever nonsense he’s spouting. \n\nAuthor: Crewman Kappa";
        security_file_headers[1][0][6] = "DDF Security Log 02:45 05/26/0017";
        security_file_transcripts[1][0][6] = "Just came across a… situation. There are five, fully armed Warframes fighting each other right now over Pandora’s Key. And they’re standing right by the only alarm button in the building. \n\nAuthor: Crewman Kappa";
        security_file_headers[1][0][7] = "DDF Security Log 07:47 12/19/0001";
        security_file_transcripts[1][0][7] = "Received word from our scout craft near Pluto that they have recovered a small red Void Key, drifting near the wreckage of a great Orokin Tower. On the assumption that it is what we have been searching for, it is in now in transit to here, escorted by as many vessels as the High Command could spare on such short notice. \n\nAuthor: Dr. Tyl Regor";

        // Difficulty: 2
        security_file_headers[1][1][0] = "DDF Security Log 09:40 05/26/0017";
        security_file_transcripts[1][1][0] = "Re: Required effort \n\nOf course you need to be double-checking your work. Really, one wonders how you even made it through the entrance exams with that attitude. \n\nAuthor: Crewman Kappa {ERROR: Message originates from ‘Cressa Tal’, not kappaman@gmail.com. Contact the IT department for questions about messaging system errors.}";
        security_file_headers[1][1][1] = "DDF Security Log 12:01 12/25/0001";
        security_file_transcripts[1][1][1] = "Received the package from JPLC today. It is indeed what we’ve been looking for. Great work on the part of everyone involved! \n\nAuthor: Dr. Tyl Regor";
        security_file_headers[1][1][2] = "DDF Security Log 11:17 03/22/0008";
        security_file_transcripts[1][1][2] = "Another note concerning Dr. Regor. What is the point of this portal that he’s trying to build? Nothing about that key makes me think we should be opening whatever it locks. \n\nAuthor: Crewman Kappa";
        security_file_headers[1][1][3] = "DDF Security Log 09:27 03/03/0003";
        security_file_transcripts[1][1][3] = "Talked to the “good” doctor today. He’s incredibly excited about having opened a tiny Void portal with that red key. I don’t understand why that’s important, we open much larger portals all the time with normal Void keys. \n\nAuthor: Crewman Kappa";
        security_file_headers[1][1][4] = "DDF Security Log 12:17 01/01/0001";
        security_file_transcripts[1][1][4] = "Re: Accommodate the good doctor \n\nI will call that filthy clone a person, but only because you’re forcing me to. I may have to work with the… person who was personally responsible for the slaughter of many of my friends, but I don’t have to like it. \n\nAuthor: Crewman Kappa";
        security_file_headers[1][1][5] = "DDF Security Log 05:03 10/13/0015";
        security_file_transcripts[1][1][5] = "Re: Information on Cressa Tal \n\nGeneral Cressa Tal was a massive pain in my side a long, long time ago. Before the Collapse. Before she went rogue and took a quarter of our forces with her. She and General Sargus saw eye to eye on almost every issue. Neither appreciated me and my research.  \n\nAuthor: Dr. Tyl Regor";
        security_file_headers[1][1][6] = "DDF Security Log 00:03 01/01/0002";
        security_file_transcripts[1][1][6] = "The Void key is curious. There are no symbols on it anywhere. Nothing to indicate the power it holds, nothing except the fact that it is pure red where most such Orokin keys are golden. Perhaps to denote the level of care to take with its use? Or perhaps to signify the level of destruction it can cause? \n\nAuthor: Dr. Tyl Regor";
        security_file_headers[1][1][7] = "DDF Security Log 02:47 05/26/0017";
        security_file_transcripts[1][1][7] = "They’re still going at it. Four of them are very distinctive: a Nyx class, a Limbo, a Nova, and a Mesa. But the one they’re fighting is very strange. It’s like an Excalibur or Ash class Warframe, but not quite. Pitch black, with a few red symbols on its face. Only thing I’m certain of is that joining this fight is suicide. \n\nAuthor: Crewman Kappa";

        // Difficulty: 3
        security_file_headers[1][2][0] = "DDF Security Log 10:29 05/26/0017";
        security_file_transcripts[1][2][0] = "Re: Unreasonable Boss \n\nTry to stop blowing up transport shuttles! Everyone is getting really uncomfortable with how high the “accidental” death count has been the past few weeks. Call me if you need help with your calculations instead of just fudging numbers. \n\nAuthor: Crewman Kappa {ERROR: Message originates from ‘Cressa Tal’, not kappaman@gmail.com. Contact the IT department for questions about messaging system errors.}";
        security_file_headers[1][2][1] = "DDF Security Log 19:50 06/16/0013";
        security_file_transcripts[1][2][1] = "Since when has there been an INFESTED lab here on campus? I was under the impression that the Technocyte Virus was cleansed from the system and the remnants sealed. How would Regor have gotten his hands on THAT?! \n\nAuthor: Crewman Kappa";
        security_file_headers[1][2][2] = "DDF Security Log 23:12 11/11/16";
        security_file_transcripts[1][2][2] = "Re: Supervillains stick together \n\nYou’re hardly a supervillain. However, I’m too busy to have you killed right now, so feel free to join the team. Although, if you ever ask me a personal question again, I’ll feed you to the Leapers myself. \n\nAuthor: Dr. Tyl Regor";
        security_file_headers[1][2][3] = "DDF Security Log 05:19 10/13/0015";
        security_file_transcripts[1][2][3] = "Re: Information on Cressa Tal \n\nCressa and Sargus both believed in honor. Cressa also believed in protecting those who could not protect themselves. Sargus valued following orders and saving his soldiers’ lives over minimizing civilian casualties. That clash drove them apart, but I think they still have some respect for each other. Since I don’t believe in honor, I had a less...genial relationship with those two.  \n\nAuthor: Dr. Tyl Regor";
        security_file_headers[1][2][4] = "DDF Security Log 09:32 03/03/0003";
        security_file_transcripts[1][2][4] = "Managed to open the first portal! Just small enough to let part of a Charger through and sever it. The Technocyte I recovered will be invaluable to my “augmentation” research.  \n\nAuthor: Dr. Tyl Regor";
        security_file_headers[1][2][5] = "DDF Security Log 12:31 01/01/0001";
        security_file_transcripts[1][2][5] = "Re: Accommodate the good doctor \n\nWhy is he here, anyway? Don’t tell me he’s some kind of idealistic augmentor who serves whoever currently embraces his vision of the future. I would bet my life he has ulterior motives for being here. \n\nAuthor: Crewman Kappa";
        security_file_headers[1][2][6] = "DDF Security Log 02:55 05/26/0017";
        security_file_transcripts[1][2][6] = "Looks like the solo Warframe is in a bad position. It’s distracted by the three in front, and the Nyx might be able to mind control it. Any second now...wait...what is the mystery one doing WAI|||||||||||||||||||%BJUI^Entry Terminated: ERROR 304 (Signal not found). \n\nAuthor: ERROR 404 (Not Found)";
        security_file_headers[1][2][7] = "DDF Security Log 00:47 01/01/0002";
        security_file_transcripts[1][2][7] = "Regardless, this is certainly Pandora’s Key. The key used to seal away the great virus near the end of the War. Now I have only to create a proper portal for the creatures trapped inside to be released upon the unsuspecting Corpus. \n\nAuthor: Dr. Tyl Regor";

        // Difficulty: 4
        security_file_headers[1][3][0] = "DDF Security Log 11:10 05/26/0017";
        security_file_transcripts[1][3][0] = "Re: OSPREY PDR documents \n\nHydrogen cells are worth considering as an element of the new combat drone. Report back when done researching them. \n\nAuthor: Crewman Kappa {ERROR: Message originates from ‘Cressa Tal’, not kappaman@gmail.com. Contact the IT department for questions about messaging system errors.}";
        security_file_headers[1][3][1] = "DDF Security Log 21:20 01/30/0017";
        security_file_transcripts[1][3][1] = "Finally found out the name of that key Regor’s had for over a decade. He calls it ‘Pandora’s Key’, and laughed for several minutes straight when I said that I didn’t get the reference. Couldn’t find anything about Pandora online, so I might have been wrong. Maybe the thing is harmless after all. \n\nAuthor: Crewman Kappa";
        security_file_headers[1][3][2] = "DDF Security Log 12:00 05/24/0017";
        security_file_transcripts[1][3][2] = "Took a look in Dr. Regor’s physics lab while he was out to lunch. That’s one strange device. Pitch black, with some kind of ancient lettering on both sides. Dunno why he put it there; after all, it’s a device he literally printed. \n\nAuthor: Crewman Kappa";
        security_file_headers[1][3][3] = "DDF Security Log 05:19 05/25/0017";
        security_file_transcripts[1][3][3] = "Re: Need the portal device now \n\nWhy did you move up the timetable? It’s not ready! For one thing, the current iteration can actually seal the void pocket permanently. If you’re not careful, you could destroy everything we’ve worked for. \n\nAuthor: Dr. Tyl Regor";
        security_file_headers[1][3][4] = "DDF Security Log 11:34 05/24/0017";
        security_file_transcripts[1][3][4] = "Finished the most recent iteration of what I’m calling Pandora’s Portal. One side will permanently open a giant portal from the currently sealed Void pocket to Earth. The subsequent flood of Infested will wipe out all Corpus here, securing the Grineer victory at last. The other side is… glitchy. It’s supposed to close the portal so that no heroes toss a nuke into in or anything. However, its actual function currently appears to be permanently sealing the Void pocket, based on the tests I ran. I’m not sure how to get rid of that functionality, as it seems inherent to the Pandora’s Key interface. \n\nAuthor: Dr. Tyl Regor";
        security_file_headers[1][3][5] = "DDF Security Log 11:37 05/24/0017";
        security_file_transcripts[1][3][5] = "Forgot to denote how to differentiate the actual sides: the right one reads PANDORAN SIGILLUM, the left one reads STULTITIA PANDORA. Any researcher worth their salt should know Latin and be able to take it from here. \n\nAuthor: Dr. Tyl Regor";
        security_file_headers[1][3][6] = "DDF Security Log 08:17 05/25/0017";
        security_file_transcripts[1][3][6] = "Re: Need the portal device now \n\nI don’t care if you’re the literal bogeyman of the system. I won’t let you have the portal device until it’s ready, and I’m sure your friend Cressa won’t let you have it at all if she finds out. Be patient! \n\nAuthor: Dr. Tyl Regor";
        security_file_headers[1][3][7] = "DDF Security Log 06:55 05/26/0017";
        security_file_transcripts[1][3][7] = "Well, that was fun. I wake up with a massive headache, a trashed building, and now both Pandora’s Key AND the portal Dr. Regor was building are missing. I don’t even know which of those two groups won the fight and took our property. Great. Good. Good great wonderful. Just peachy. Can’t wait to explain this to that blasted Grineer when he shows up a couple hours from now. \n\nAuthor: Crewman Kappa";

        // Next come the EWF logs
        // Difficulty: 1
        security_file_headers[2][0][0] = "EWF Security Log 08:55 05/26/0017";
        security_file_transcripts[2][0][0] = "Re: Reports of campus intruders \n\nThank you for your concern, I’ll be fine. \n\nAuthor: Crewman Zeta Tau {ERROR: Message originates from ‘Cressa Tal’, not zetaman1@gmail.com. Contact the IT department for questions about messaging system errors.}";
        security_file_headers[2][0][1] = "EWF Security Log 13:13 06/01/0013";
        security_file_transcripts[2][0][1] = "Noting potential psych issue: the “Sergeant” appears to be obsessed with that weird ancient sport people played centuries ago. I think it was called “basketball”? \n\nAuthor: Crewman Zeta Tau";
        security_file_headers[2][0][2] = "EWF Security Log 21:37 11/02/0014";
        security_file_transcripts[2][0][2] = "Noticed a scientist down here working on something that looked very Grineer. I’m unsure if it’s a captured weapon or not… it looked too new and experimental to be captured tech. Just noting it because paranoia. \n\nAuthor: Crewman Zeta Nu";
        security_file_headers[2][0][3] = "EWF Security Log 07:55 12/25/0016";
        security_file_transcripts[2][0][3] = "My former subordinate’s sense of humor has not changed in the slightest. Bucket of water over a doorway as a Christmas present? This I can deal with. But a GLAXION? That’s going too far. Zeta Nu almost got frozen solid. \n\nAuthor: Crewman Zeta Tau";
        security_file_headers[2][0][4] = "EWF Security Broadcast 02:00 05/26/0013";
        security_file_transcripts[2][0][4] = "Return alert level to 4. Those weren’t Grineer napalm bombs, just the Sergeant running some “experiments” with an antique…”microwave”? and a few ammo crates. \n\nAuthor: Crewman Zeta Nu";
        security_file_headers[2][0][5] = "EWF Security Log 12:00 07/03/0015";
        security_file_transcripts[2][0][5] = "Re: Why are we patrolling this building? \n\nYes, you are correct that there is, to everyone’s knowledge, one solitary Grineer on this entire planet. And I would appreciate you not mentioning that to our bosses. This is literally the best job in the system. \n\nAuthor: Crewman Zeta Nu";
        security_file_headers[2][0][6] = "EWF Security Log 12:01 07/03/0015";
        security_file_transcripts[2][0][6] = "Re: What’s wrong with the Sergeant? \n\nI think he, for some reason, just really likes the idea of being a hero. Winning is super important to him. Personally, staying alive seems way more important. \n\nAuthor: Crewman Zeta Tau";
        security_file_headers[2][0][7] = "EWF Security Log 17:00 05/25/0017";
        security_file_transcripts[2][0][7] = "Re: VIP on campus: \n\nwould. You. care. TO. EXPLAIN. WHY SARGUS tarnation tarnation tarnation tarnation RUK is going to be touring MY building tomorrow? \n\nAuthor: Crewman Zeta Tau";

        // Difficulty: 2
        security_file_headers[2][1][0] = "EWF Security Log 09:40 05/26/0017";
        security_file_transcripts[2][1][0] = "Re: Strange noises from the basement floor \n\nRight, I’ll check on that. Although, if this is another one of your pranks, I’ll file another complaint with your superiors. In the event that they continue to not care...whatever. Nevermind, just ignore my rambling. \n\nAuthor: Crewman Zeta Tau {ERROR: Message originates from ‘Cressa Tal’, not zetaman1@gmail.com. Contact the IT department for questions about messaging system errors.}";
        security_file_headers[2][1][1] = "EWF Security Log 13:02 05/26/0016";
        security_file_transcripts[2][1][1] = "Re: WHY IS HE AT THE LIBRARY? \n\nI thought it would be a good way to get rid of him, all right? I thought he’d just go there, learn more about his favorite sport, and leave us alone. How was I supposed to know he’d turn into this super nerd? \n\nAuthor: Crewman Zeta Tau";
        security_file_headers[2][1][2] = "EWF Security Log 02:05 03/18/0013";
        security_file_transcripts[2][1][2] = "Finally got a delivery of cookies. Can finally patrol in peace without the self-styled “Sergeant” constantly yelling, “Where are the cookies?”. For a couple hours there, I think that soldier would have helped the Grineer themselves just to get a single chocolate chip delicacy. \n\nAuthor: Crewman Zeta Nu";
        security_file_headers[2][1][3] = "EWF Security Log 13:02 07/06/0016";
        security_file_transcripts[2][1][3] = "Re: What is/was an “Averite”? \n\nSeem to recall hearing a visiting historian talk about them. Something to do with Cortech’s history before the Collapse. Just tell the Sergeant they were really good at basketball and he’ll leave you alone. \n\nAuthor: Crewman Zeta Nu";
        security_file_headers[2][1][4] = "EWF Security Log 17:05 05/25/0017";
        security_file_transcripts[2][1][4] = "Re: VIP on campus: \n\nI DON’T tarnation CARE if the tarnation tarnation tarnation tarnation tarnation Sergeant \n\nAuthorized him to be here. When has the Sergeant done ANYTHING that would give him trustworthiness?! \n\nAuthor: Crewman Zeta Tau";
        security_file_headers[2][1][5] = "EWF Security Log 17:06 05/25/0017";
        security_file_transcripts[2][1][5] = "Re: VIP on campus: \n\nPlease stop shouting! You know these things record our patrol shifts, right? Even if he is capable of levelling half of campus in minutes, we don’t have a choice in the matter. All we can do is figure out contingency plans.";
        security_file_headers[2][1][6] = "EWF Security Log 19:14 05/25/0017";
        security_file_transcripts[2][1][6] = "All preparations made for the tarnation Iron Man of the Grineer to show up. Mines in the floor, MOAs in the walls, and I even got AEDF to loan us out a BURSA to put in the elevator. We’ll honor his safe passage... for exactly as long as he makes no aggressive motions whatsoever. \n\nAuthor: Crewman Zeta Nu";
        security_file_headers[2][1][7] = "EWF Security Log 14:15 08/08/0016";
        security_file_transcripts[2][1][7] = "Re: Should we report him? \n\nI think the Sergeant was joking about being a supervillain. I mean, seriously, no one would do the stuff he was talking about for that trivial of a reason. Maybe make a note of it at some point, though. \n\nAuthor: Crewman Zeta Tau";

        // Difficulty: 3
        security_file_headers[2][2][0] = "EWF Security Log 10:29 05/26/0017";
        security_file_transcripts[2][2][0] = "In this building, we don’t worry about attack by the Grineer. Not while the Sergeant has nothing better to do than troll us all day long. \n\nAuthor: Crewman Zeta Nu {ERROR: Message originates from ‘Cressa Tal’, not zetaman2@gmail.com. Contact the IT department for questions about messaging system errors.}";
        security_file_headers[2][2][1] = "EWF Security Log 22:33 10/05/0016";
        security_file_transcripts[2][2][1] = "Starting to be slightly more concerned about what’s going on in this building. That one scientist has been working on his suspicious project for two years at least now. I’m 90% certain he’s CREATING Grineer tech, not reverse-engineering it. Don’t know what to do about this except log it; my current superior is hardly a good person to talk to about this. \n\nAuthor: Crewman Zeta Nu";
        security_file_headers[2][2][2] = "EWF Security Log 12:05 08/1/0016";
        security_file_transcripts[2][2][2] = "Re: Sergeant going insane \n\nHow was I supposed to know that these “Averites” were actually really bad at basketball? Why does it even matter? It’s not like he’s one of them… he’s hardly old enough to have been born during the end of the Collapse. \n\nAuthor: Crewman Zeta Nu";
        security_file_headers[2][2][3] = "EWF Security Log 16:09 10/05/0015";
        security_file_transcripts[2][2][3] = "Re: Do you have a girlfriend? \n\nMy sincere apologies, Dr. Regor. The Sergeant came down here for a routine inspection today; I didn’t realize he was using our comms to message you these personal questions until it was too late. We’ll endeavor to make sure this never happens again. Again, my sincere apologies. Please don’t kill us. \n\nAuthor: Crewman Zeta Tau";
        security_file_headers[2][2][4] = "EWF Security Log 16:01 10/05/0015";
        security_file_transcripts[2][2][4] = "Re: Pranking Zeta Tau \n\nThis actually sounds hilarious, just be careful who you send the messages to. Some people on this campus are literally trigger-happy. \n\nAuthor: Crewman Zeta Nu";
        security_file_headers[2][2][5] = "EWF Security Log 22:50 10/05/0016";
        security_file_transcripts[2][2][5] = "Researched that one scientist a bit more. Why exactly do we employ someone whose funding comes from a non-Corpus source? Seems like a serious security loophole.  \n\nAuthor: Crewman Zeta Nu";
        security_file_headers[2][2][6] = "EWF Security Log 16:13 10/05/0015";
        security_file_transcripts[2][2][6] = "Re: Unauthorized Messages \n\nThe Sergeant sent messages to multiple Grineer today, using my comlink without my knowledge. I’d like to state for the record that I had no intention of sending that kind of treasonous content, and have every intention to make sure it never happens again. \n\nAuthor: Crewman Zeta Tau";
        security_file_headers[2][2][7] = "EWF Security Log 07:13 05/26/0017";
        security_file_transcripts[2][2][7] = "Re: Unauthorized Gunfire \n\nNext time, don’t invite Grineer generals into my data vault! Next time, don’t invite the kind of people that waltz in, put on “experimental” tech, and then proceed to blast their way out of the building! Next time, think about who you’re giving safe passage to!! \n\nAuthor: Crewman Zeta Tau";

        // Difficulty: 4
        security_file_headers[2][3][0] = "EWF Security Log 11:10 05/26/0017";
        security_file_transcripts[2][3][0] = "Re: Given how today’s going, how about an early lunch break? \n\nGet your act together, soldier. \n\nAuthor: Crewman Zeta Nu {ERROR: Message originates from ‘Cressa Tal’, not zetaman2@gmail.com. Contact the IT department for questions about messaging system errors.}";
        security_file_headers[2][3][1] = "EWF Security Log  01:59 05/023/0017";
        security_file_transcripts[2][3][1] = "Took a casual peek inside the suspicious scientist’s lab today. That tech he’s working on is as Grineer as it gets. It’s quite rough, but looks highly effective. I’ll file an official report soon. This seems incredibly dangerous to have lying around, especially as I’m quite sure that’s not what he’s supposed to be researching. \n\nAuthor: Crewman Zeta Nu";
        security_file_headers[2][3][2] = "EWF Security Log 01:20 05/26/0017";
        security_file_transcripts[2][3][2] = "Re: Sargus weaknesses \n\nIntelligence suggests three separate weak points. Chest, back and that giant nuclear reactor. Stay out of the way of his rocket launcher, and we’ll be just fine. There’s no way he can make it out of this facility with that many weak points. -Crewman Zeta Tau";
        security_file_headers[2][3][3] = "EWF Security Log 05:37 05/26/0017";
        security_file_transcripts[2][3][3] = "Re: Why is the Sergeant up at this hour? \n\nI don’t know. Maybe he want to be useful for once and help make sure Sargus doesn’t end our world? \n\nAuthor: Crewman Zeta Nu";
        security_file_headers[2][3][4] = "EWF Security Log 10:12 12/24/0016";
        security_file_transcripts[2][3][4] = "Re: Sergeant stuff \n\nOk, that is actually interesting. Can you ask him why it used to be called “Bridge”? I mean, it used to be a physics building. Its current name makes so much more sense \n\nAuthor: Crewman Zeta Tau";
        security_file_headers[2][3][5] = "EWF Security Log 10:58 12/24/0016";
        security_file_transcripts[2][3][5] = "Making a note of the Sergeant’s erratic behavior. Pretty sure threatening to end the world over some random dormitory being bad at some ancient sport is the literal definition of insane. \n\nAuthor: Crewman Zeta Nu";
        security_file_headers[2][3][6] = "EWF Security Log 07:50 05/26/0017";
        security_file_transcripts[2][3][6] = "Update on the Sergeant in the wake of this morning’s incident: MIA. We haven’t heard from him since Sargus turned half the labs into rubble. Haven’t seen his corpse, don’t know where he is. \n\nAuthor: Crewman Zeta Nu";
        security_file_headers[2][3][7] = "EWF Security Log 07:55 05/26/0017";
        security_file_transcripts[2][3][7] = "Update on Sargus Ruk in the wake of this morning’s incident: Completely invincible. He has no weak points anywhere, except for a nuclear exhaust port on his back. Armament: Energy Shotgun and Elbow Shield created by one of our very own, now very dead, EWF scientists. \n\nAuthor: Crewman Zeta Tau";

        // Last come the IRF logs
        // Difficulty: 1
        security_file_headers[3][0][0] = "IRF Security Broadcast 08:55 05/26/0017";
        security_file_transcripts[3][0][0] = "Find a working alarm, and fast. All of the basement labs just suffered a simultaneous blackout, and none of the alarms are working down here. Can’t have our researchers getting killed by their research, now can we? \n\nAuthor: Crewman Omega {ERROR: Message originates from ‘Cressa Tal’, not megaman@gmail.com. Contact the IT department for questions about messaging system errors.}";
        security_file_headers[3][0][1] = "IRF Security Log 12:49 07/30/0016";
        security_file_transcripts[3][0][1] = "Why is Crewman Tau so paranoid all the time? It’s been 16 years since the Collapse. We haven’t been actively fighting the Grineer for 5. Who does he think is going to show up and cause problems, the Tenno or something? \n\nAuthor: Crewman Epsilon";
        security_file_headers[3][0][2] = "IRF Security Log 12:33:03/20/0016";
        security_file_transcripts[3][0][2] = "Re: Why are there INFESTED at this campus? \n\nDid you seriously never actually wonder what the acronym of the building you’ve been guarding stands for? This has been a thing since before you time, thanks to Dr Tyl Regor. \n\nAuthor: Crewman Epsilon";
        security_file_headers[3][0][3] = "IRF Security Log 03:33 01/21/0017";
        security_file_transcripts[3][0][3] = "I have to report this, no matter what Crewman Epsilon thinks about my sanity. I keep seeing shadows moving out of the corners of my eyes. It’s NOT the rats. Someone...something is stalking us. \n\nAuthor: Crewman Tau";
        security_file_headers[3][0][4] = "IRF Security Log 00:19 12/25/0016";
        security_file_transcripts[3][0][4] = "Re: Stalker Are you serious dude? \n\nThe Stalker is a legendary assassin from the War. First off, why would he even be on campus? There are no Tenno here, and everyone know that particular killer only hunts Tenno. \n\nAuthor: Crewman Epsilon";
        security_file_headers[3][0][5] = "IRF Security Log 14:30 04/12/0017";
        security_file_transcripts[3][0][5] = "Received the new patrol suits today. The traitor scientist delivered them himself, with that strange smirk on his face the entire time. Crazy or not, though, his tech had better keep us from ending up like Crewman Epsilon. \n\nAuthor: Crewman Tau";
        security_file_headers[3][0][6] = "IRF Security Log 01:01 05/19/0017";
        security_file_transcripts[3][0][6] = "Crewman Tau hasn’t returned to his post for two hours. Not sure what’s going on; thought I noticed him lying down by the exit passage, but found nothing when walking over to check. Might be connected to the rash of recent disappearances. \n\nAuthor: Crewman Zeta";
        security_file_headers[3][0][7] = "IRF Security Log 01:15 05/19/0017";
        security_file_transcripts[3][0][7] = "Disregard previous entry. Crewman Tau has returned and should give an explanation of himself shortly. Wait… that’s NOT Crewanijadgn|||||||||||||INFES|||||||Entry Terminated: ERROR 304 (Signal not found). \n\nAuthor: ERROR 404 (Not Found)";

        // Difficulty: 2
        security_file_headers[3][1][0] = "IRF Security Log 09:40 05/26/0017";
        security_file_transcripts[3][1][0] = "Re: Missing Salt Crates \n\nIodine deficiency in subjects is a POSITIVE element in your conversion research? See, you could have told us that BEFORE we destroyed all of your “specially treated” salt... \n\nAuthor: Crewman Alpha {ERROR: Message originates from ‘Cressa Tal’, not xXalphaman75631Xx@gmail.com. Contact the IT department for questions about messaging system errors.}";
        security_file_headers[3][1][1] = "IRF Security Broadcast 00:01 04/04/0017";
        security_file_transcripts[3][1][1] = "Holding cell breach. I repeat, we have a holding cell breach. All guards are advised to seek shelter immediately and ready weapons. All non-combat personnel should shelter in place immediately and stay as still as humanly possible. \n\nAuthor: Dr. Tyl Regor";
        security_file_headers[3][1][2] = "IRF Security Log 16:03 05/25/0017";
        security_file_transcripts[3][1][2] = "The interesting thing is that while Crewman Tau, after becoming Infested, killed Crewman Zeta, Crewman Zeta did NOT himself turn into infested. Whatever strain of Technocyte this is, it’s not likely to cause an epidemic. \n\nAuthor: Crewman Alpha";
        security_file_headers[3][1][3] = "IRF Security Log 00:03 04/04/0017";
        security_file_transcripts[3][1][3] = "Found Crewman Epsilon on the outskirts of his patrol tonight. He’s been slashed in multiple places by some sharp object and isn’t breathing. I need a medic and backup ASAP. please hurry i don’t want to be alone out here. \n\nAuthor: Crewman Tau";
        security_file_headers[3][1][4] = "IRF Security Log 14:10 04/12/0017";
        security_file_transcripts[3][1][4] = "Took delivery of 2 prototype patrol suits from Dr. Tyl Regor. \nDetails: \n\nFully airtight, heavily reinforced seams, self-sealing insulation. \nSpecial feature: Reverse-engineered Retribution mod. \n\nAuthor: Crewman Zeta";
        security_file_headers[3][1][5] = "IRF Security Log 15:17 04/12/0017";
        security_file_transcripts[3][1][5] = "Re: Heard about the incident \n\nDon’t worry about me, dear. We just got all-new suits to keep something like that from happening again. I’ll be fine. Love you too. \n\nAuthor: Crewman Tau";
        security_file_headers[3][1][6] = "IRF Security Log 14:35 04/12/0017";
        security_file_transcripts[3][1][6] = "Just looking through the suit before putting it on: the Retribution mod seems a little exposed. I mean, it’s inside the suit and all, but it itself is not armored in the slightest. Still better than what we had before.  \n\nAuthor: Crewman Tau";
        security_file_headers[3][1][7] = "IRF Security Log 00:26 12/25/0016";
        security_file_transcripts[3][1][7] = "Re: Stalker \n\nOk, yes, there were some Tenno recruited from Cortech pre-Collapse. So you tell me, what’s the likelihood that they were cryogenically preserved here through the ENTIRE War AND that the STALKER knows about them AND that he showed up here 30 minutes ago? Seriously, stop being so paranoid. You’re not a Tenno, after all, what’s the issue? \n\nAuthor: Crewman Epsilon";

        // Difficulty: 3
        security_file_headers[3][2][0] = "IRF Security Log 10:29 05/26/0017";
        security_file_transcripts[3][2][0] = "What idiot dropped elemental lithium into one of the holding pens? The explosion almost blew the doors off. No one needs a wild Leaper running around campus. It would raise FAR too many questions. \n\nAuthor: Crewman Alpha {ERROR: Message originates from ‘Cressa Tal’, not xXalphaman75631Xx@gmail.com. Contact the IT department for questions about messaging system errors.}";
        security_file_headers[3][2][1] = "IRF Security Log 15:19 04/12/0017";
        security_file_transcripts[3][2][1] = "Re: Heard about the incident \n\nSeriously, that’s just my silly paranoia. I’m joking! There are no assassins around here. \n\nAuthor: Crewman Tau";
        security_file_headers[3][2][2] = "IRF Security Log 16:09 05/25/0017";
        security_file_transcripts[3][2][2] = "The real question is, how did whatever killed Crewman Tau get past his electro-shock suit? The only real answer we have to that is a deep triangular wound in the corpse. Reconstructing the way his suit was positioned will take time, but it is likely that a projectile of some kind actually destroyed his Retribution mod before he was turned. \n\nAuthor: Crewman Omega";
        security_file_headers[3][2][3] = "IRF Security Log 00:39 12/25/0016";
        security_file_transcripts[3][2][3] = "Re: Stalker \n\nWhy would the STALKER be collaborating with the Sergeant? To my knowledge, the Sergeant is super young, and a normal human. Admittedly, can’t always tell just by appearances...still, that’s really farfetched. \n\nAuthor: Crewman Epsilon";
        security_file_headers[3][2][4] = "IRF Security Log 07:12 04/04/0017";
        security_file_transcripts[3][2][4] = "Finally tracked down and killed the Infested that killed Epsilon. Dr. Regor wanted me to bring it in alive, and I...diplomatically said no. \n\nAuthor: Crewman Tau";
        security_file_headers[3][2][5] = "IRF Security Log 23:00 05/18/0017";
        security_file_transcripts[3][2][5] = "Re: How are the suits working out? \n\nSo far, pretty great. I haven’t had any issues with getting h|||||||||||||||||67yuhjnmkEntry Terminated: ERROR 304 (Signal not found). \n\nAuthor: ERROR 404 (Not Found)";
        security_file_headers[3][2][6] = "IRF Security Log 16:44 05/25/0017";
        security_file_transcripts[3][2][6] = "Found a discarded needle near where Zeta’s transcripts indicate Tau was killed. Traces of Technocyte are on the needle. \n\nAuthor: Crewman Omega";
        security_file_headers[3][2][7] = "IRF Security Log 16:45 05/25/0017";
        security_file_transcripts[3][2][7] = "While waiting on the reconstruction software, we decided to do some digging. It turns out that the projectile was still buried in the corpse: an 8-10 cm throwing knife. Whoever initiated this attack was definitely using a Warframe. \n\nAuthor: Crewman Omega";

        // Difficulty: 4
        security_file_headers[3][3][0] = "IRF Security Log 11:10 05/26/0017";
        security_file_transcripts[3][3][0] = "Re: Will be sending replacements shortly so that your wounds can be treated. \n\nTHANK. YOU. \n\nAuthor: Crewman Omega {ERROR: Message originates from ‘Cressa Tal’, not megaman@gmail.com. Contact the IT department for questions about messaging system errors.}";
        security_file_headers[3][3][1] = "IRF Security Log 00:26 05/18/0017";
        security_file_transcripts[3][3][1] = "Did some sleuthing in the library. According to the one source I could find, the Stalker was most likely of Cortech origin. He does have ties to this campus. It’s not impossible that he might return. \n\nAuthor: Crewman Tau";
        security_file_headers[3][3][2] = "IRF Security Log 19:59 05/25/0017";
        security_file_transcripts[3][3][2] = "Re: Can you send the message to Crewman Tau’s family? \n\nPlease don’t put this on me, man. I don’t even know what to say.  \n\nAuthor: Crewman Omega";
        security_file_headers[3][3][3] = "IRF Security Log 00:28 05/18/0017";
        security_file_transcripts[3][3][3] = "Found out a couple more things. First, there were indeed a group of Tenno recruited from Cortech pre-Collapse. Apparently from some kind of housing unit called “Avery”? The other thing is that the Sergeant seems to really care about that place, based on what I heard him arguing about with Crewman Zeta Nu. \n\nAuthor: Crewman Tau";
        security_file_headers[3][3][4] = "IRF Security Log 00:01 12/25/0016";
        security_file_transcripts[3][3][4] = "Just saw something super strange. Was walking on my normal patrol path and noticed the Sergeant standing by the north side of IRF. What was weird was that he had two shadows. And he was talking to one of them. Crewman Epsilon doesn’t believe me, of course. \n\nAuthor: Crewman Tau";
        security_file_headers[3][3][5] = "IRF Security Log 21:12 05/25/0017";
        security_file_transcripts[3][3][5] = "The one thing we really don’t understand is WHY Crewman Tau was targeted. Maybe he knew something he shouldn’t have? \n\nAuthor: Crewman Omega";
        security_file_headers[3][3][6] = "IRF Security Log 04:25 05/26/0017";
        security_file_transcripts[3][3][6] = "We’ve come to the conclusion that the deceased Crewman Tau was turned into some kind of Infested, using a quick-acting strain of the Technocyte virus. Given that there is only one source for that kind of material on campus, Dr. Tyl Regor should be arrested as soon as he shows up today. \n\nAuthor: Crewman Omega";
        security_file_headers[3][3][7] = "IRF Security Log 23:59 05/25/0017";
        security_file_transcripts[3][3][7] = "Re: What happened? \n\nI’m sorry ma’am. It turns out that your husband was right. There are conspirators on campus. As far as we can tell, he was killed for having found out something about them. We’re dealing with the situation as we speak, and will get back to you with culprits ASAP. \n\nAuthor: Crewman Alpha";
    }

    @Override
    public void onBackPressed() {
    }
}
