package com.example.cole.thirdattempt;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import static android.R.attr.translationY;

public class HackingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hacking);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        final ImageView img_animation = (ImageView) findViewById(R.id.hacking_pick);
        final ImageView lock_rep = (ImageView) findViewById(R.id.hacking_lock);


        final ValueAnimator animator = ValueAnimator.ofFloat(0.0f, 1.0f);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setInterpolator(new LinearInterpolator());
        animator.setDuration(10000L);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                final float progress_degrees = (float) animation.getAnimatedValue();
                final float radius = lock_rep.getWidth() / 2;
                final float translationX = radius * (float) (Math.cos(2 * Math.PI * progress_degrees));
                final float translationY = radius * (float) (Math.sin(2 * Math.PI * progress_degrees));
                img_animation.setTranslationX(translationX);
                img_animation.setTranslationY(translationY);
                img_animation.setRotation((float) (90 + 360.0 * progress_degrees));
            }
        });
        animator.start();

    }

    /** Called when the user taps the Send button */
    public void go_to_Codex(View view) {
        Intent intent = new Intent(this, Codex.class);
        startActivity(intent);
    }


}
