package com.example.cole.thirdattempt;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.os.SystemClock;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Handler;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.util.Date;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import static android.view.View.INVISIBLE;
import static java.lang.Integer.parseInt;
import static java.lang.Integer.toString;


public class TerminalActivity extends AppCompatActivity {
    //    private final String DEVICE_NAME="MyBTBee";
    final String Terminal_1_ADDRESS="20:16:09:08:18:94";
    final String Terminal_2_ADDRESS="20:16:11:02:03:84";
    final String Terminal_3_ADDRESS="20:16:12:12:70:07";
    final String Terminal_4_ADDRESS="20:16:12:12:69:07";
    private final UUID PORT_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");//Serial Port Service ID
    private BluetoothDevice device;
    private BluetoothSocket socket;
    private OutputStream outputStream;
    private InputStream inputStream;



    String decode_string = ""; // string for receiving and decoding signals from the Arduino terminals
    Boolean start_pressed = false;
    int start_counter = 0;
    int current_terminal = 1;
    Button startButton, stopButton, hackButton, lockButton, trueLockButton, returnButton, continueButton,
            codexButton, titleButton, practiceButton, downloadButton;
    SeekBar difficultySlider;
    ProgressBar downloadBar;
    TextView textView, hack_timer_text;
    ImageView pick, lock;

    float speed_multiplier = 1;
    Random dice = new Random();
    int[] bolt_statuses;    // Whether bolt has been pushed
    int[] bolt_directions;    // Adding or subtracting from bolt position
    boolean[] bolt_activations;    // Enabling bolts
    float[] bolt_positions; // Position of bolt relative to starting position
    ImageView[] bolts;

    int alert_counter; // Keeps track of how many alerts have been set off
    long decay_counter; // Keeps track of how many milliseconds of time have passed since boot
    float alert_alpha; // Keeps track of alert mark transparency
    ImageView[] open_marks;
    ImageView[] filled_marks;

    long hack_timer; // Keeps track of how long it takes to make it through all hacks

    int[] terminal_security_level = new int[4];
    int current_hack_level = 0;
    ImageView[] locks;
    ImageView[] unlocks;
    boolean hack_instance_completed = false;


    float download_progress = 0;
    boolean downloading = false;
    int current_terminal_difficulty_level = 1;
    int current_terminal_access_level = 1;
    int practice_level = 1;
    boolean practicing = false;

    double temp_hack_time;
    double true_hack_time;
    boolean hack_time_set;
    String temp_timer_text;

    float progress_degrees, pick_direction;
    EditText editText;
    boolean deviceConnected=false;
    Thread thread;
    byte buffer[];
    int bufferPosition;
    boolean stopThread;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terminal);
        startButton = (Button) findViewById(R.id.buttonStart);
        stopButton = (Button) findViewById(R.id.buttonStop);
        hackButton = (Button) findViewById(R.id.buttonHack);
        textView = (TextView) findViewById(R.id.textView);
        hack_timer_text = (TextView) findViewById(R.id.textTimer);
        lockButton = (Button) findViewById(R.id.buttonLock);
        trueLockButton = (Button) findViewById(R.id.buttonTrueHack);
        continueButton = (Button) findViewById(R.id.buttonContinue);
        returnButton = (Button) findViewById(R.id.buttonReturn);
        codexButton = (Button) findViewById(R.id.buttonCodex);
        titleButton = (Button) findViewById(R.id.buttonTitle);
        practiceButton = (Button) findViewById(R.id.buttonPractice);
        downloadButton = (Button) findViewById(R.id.buttonDownload);


        trueLockButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){

                    // Calling Lock/Unlock
                    Lock_or_Unlock(view);
                    return true;
                }
                return false;
            }
        });
        trueLockButton.setEnabled(false);
        trueLockButton.setVisibility(Button.INVISIBLE);
        lockButton.setEnabled(false);

        difficultySlider = (SeekBar) findViewById(R.id.sliderDifficulty);
        downloadBar = (ProgressBar) findViewById(R.id.barDownload);

        setUiEnabled(false);
        textView.setText("\nNot connected to any data terminals.\n");


        pick = (ImageView) findViewById(R.id.hacking_pick);
        lock = (ImageView) findViewById(R.id.hacking_lock);

        pick_direction = 1;

        bolt_statuses = new int[8];
        bolt_directions = new int[8];
        bolt_activations = new boolean[8];
        bolt_positions = new float[8];
        bolts = new ImageView[8];

        open_marks = new ImageView[4];
        filled_marks = new ImageView[4];


        locks = new ImageView[6];
        unlocks = new ImageView[6];

        bolts[0] = (ImageView) findViewById(R.id.tumbler_1);
        bolts[1] = (ImageView) findViewById(R.id.tumbler_2);
        bolts[2] = (ImageView) findViewById(R.id.tumbler_3);
        bolts[3] = (ImageView) findViewById(R.id.tumbler_4);
        bolts[4] = (ImageView) findViewById(R.id.tumbler_5);
        bolts[5] = (ImageView) findViewById(R.id.tumbler_6);
        bolts[6] = (ImageView) findViewById(R.id.tumbler_7);
        bolts[7] = (ImageView) findViewById(R.id.tumbler_8);
        for (int i = 0; i < 8; i++) {
            bolt_statuses[i] = 0;
            bolt_directions[i] = -1;
            bolt_activations[i] = true;
            bolt_positions[i] = 0;
        }

        alert_counter = 0;
        decay_counter = SystemClock.elapsedRealtime();
        alert_alpha = 1;
        open_marks[0] = (ImageView) findViewById(R.id.open_mark_1);
        open_marks[1] = (ImageView) findViewById(R.id.open_mark_2);
        open_marks[2] = (ImageView) findViewById(R.id.open_mark_3);
        open_marks[3] = (ImageView) findViewById(R.id.open_mark_4);
        filled_marks[0] = (ImageView) findViewById(R.id.filled_mark_1);
        filled_marks[1] = (ImageView) findViewById(R.id.filled_mark_2);
        filled_marks[2] = (ImageView) findViewById(R.id.filled_mark_3);
        filled_marks[3] = (ImageView) findViewById(R.id.filled_mark_4);

        terminal_security_level[0] = 3;
        terminal_security_level[1] = 4;
        terminal_security_level[2] = 5;
        terminal_security_level[3] = 6;

        locks[0] = (ImageView) findViewById(R.id.iconLock_1);
        locks[1] = (ImageView) findViewById(R.id.iconLock_2);
        locks[2] = (ImageView) findViewById(R.id.iconLock_3);
        locks[3] = (ImageView) findViewById(R.id.iconLock_4);
        locks[4] = (ImageView) findViewById(R.id.iconLock_5);
        locks[5] = (ImageView) findViewById(R.id.iconLock_6);

        unlocks[0] = (ImageView) findViewById(R.id.iconUnlock_1);
        unlocks[1] = (ImageView) findViewById(R.id.iconUnlock_2);
        unlocks[2] = (ImageView) findViewById(R.id.iconUnlock_3);
        unlocks[3] = (ImageView) findViewById(R.id.IconUnlock_4);
        unlocks[4] = (ImageView) findViewById(R.id.iconUnlock_5);
        unlocks[5] = (ImageView) findViewById(R.id.iconUnlock_6);

        final ValueAnimator animator = ValueAnimator.ofFloat(0.0f, 1.0f);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setInterpolator(new LinearInterpolator());
        animator.setDuration(10000L);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {

                // Running bluetooth coneect stuff
                if (start_pressed == true && start_counter < 1) {
                    start_counter++;
                }
                else if (start_pressed == true && start_counter >= 1) {
                    attempt_connection();
                    start_pressed = false;
                    start_counter = 0;
                }


                // progress_degrees = (float) animation.getAnimatedValue();
                progress_degrees += pick_direction * 0.004 * speed_multiplier;
                if (progress_degrees > 1) {
                    progress_degrees -= (float) 1;
                }
                else if (progress_degrees < 0) {
                    progress_degrees += (float) 1;
                }
                final float radius = lock.getWidth() / 2;
                final float translationX = radius * (float) (Math.cos(2 * Math.PI * progress_degrees));
                final float translationY = radius * (float) (Math.sin(2 * Math.PI * progress_degrees));
                pick.setTranslationX(translationX);
                pick.setTranslationY(translationY);
                pick.setRotation((float) (90 + 360.0 * progress_degrees));

                // Making Alert symbols decay away slowly
                long temp_time = SystemClock.elapsedRealtime(); //  Calling this method once
                int decay_period;
                if (practicing == false) {
                    decay_period = ((6 - current_terminal_difficulty_level) * 1000);
                }
                else {
                    decay_period = ((6 - practice_level) * 1000);
                }
                if (alert_counter > 0) {
                    if (temp_time - decay_counter > decay_period) {
                        decay_counter = temp_time;
                        filled_marks[alert_counter - 1].setVisibility(INVISIBLE);
                        alert_counter--;
                        alert_alpha = 1;
                    }
                    else {
                        if (temp_time - decay_counter > 0) {
                            alert_alpha = (float) (decay_period - (temp_time - decay_counter)) / (float) decay_period;
                            // Fades out the alerts slowly
                            filled_marks[alert_counter - 1].setAlpha(alert_alpha);
                        }
                        else {
                            decay_counter = temp_time;
                            alert_alpha = (float) (decay_period - (temp_time - decay_counter)) / (float) decay_period;
                        }
                    }
                }


                // Only shows up if not practicing
                 if (current_hack_level >= terminal_security_level[current_terminal_difficulty_level - 1]
                    && (downloading == true && download_progress < 100)) {
                     download_progress += 0.08 * (float)(current_terminal_difficulty_level); // Currently harder-to-access files download faster
                     downloadBar.setProgress((int) download_progress);
                 }
                 else if (current_hack_level >= terminal_security_level[current_terminal_difficulty_level - 1]
                         && (downloading == true && download_progress >= 100)){
                     downloadBar.setVisibility(INVISIBLE);
                     returnButton.setEnabled(true);
                     returnButton.setVisibility(ImageView.VISIBLE);
                 }
                // Hijacking this animation to also alter the bolt positions
                for (int j = 0; j < 8; j++) {

                    if (bolt_positions[j] >= 0 && bolt_positions[j] <= 120){
                        bolt_positions[j] += 5 * (float) bolt_directions[j];
                        if (bolt_positions[j] < 0) {
                            bolt_statuses[j] = 0;
                            bolt_positions[j] = 0;
                        }
                        else if (bolt_positions[j] > 120){
                            bolt_statuses[j] = 1;
                            bolt_positions[j] = 120;
                            if (hack_instance_completed == false) {
                                hack_instance_completed = true;
                                for (int i = 0; i < 8; i++) {
                                    if (bolt_statuses[i] != 1 && bolt_activations[i]) {
                                        hack_instance_completed = false;
                                    }
                                }
                                if (hack_instance_completed) {
                                    // Should verify with Arduino here.
                                    current_hack_level++;

                                    trueLockButton.setEnabled(false);
                                    trueLockButton.setVisibility(INVISIBLE);
                                    for (int k = 0; k < current_hack_level; k++) {
                                        locks[k].setVisibility(INVISIBLE);
                                        unlocks[k].setVisibility(ImageView.VISIBLE);
                                    }
                                    if (practicing == false) {
                                        if (current_hack_level < terminal_security_level[current_terminal_difficulty_level - 1]) {
                                            continueButton.setEnabled(true);
                                            continueButton.setVisibility(ImageView.VISIBLE);
                                        }
                                        else {
                                            temp_timer_text = "";
                                            if (hack_time_set == false) {
                                                temp_hack_time = ((SystemClock.elapsedRealtime() - hack_timer) / (float) 1000);
                                                hack_time_set = true;
                                            }
                                            String decimal_places;
                                            if (temp_hack_time != (int) temp_hack_time) {
                                                decimal_places = (String.valueOf(temp_hack_time)).substring((String.valueOf(temp_hack_time)).indexOf(".") + 1, (String.valueOf(temp_hack_time)).indexOf(".") + 3);
                                            }
                                            else {
                                                decimal_places = "00";
                                            }
                                            temp_hack_time = (int) temp_hack_time;
                                            int minute_counter = 0;
                                            while(temp_hack_time >= 60) {
                                                temp_hack_time -= 60;
                                                minute_counter++;
                                            }
                                            String minutes = String.valueOf(minute_counter);
                                            if (minutes.length() < 2) {
                                                minutes = "0" + minutes;
                                            }
                                            String seconds = String.valueOf((int) temp_hack_time);
                                            if (seconds.length() < 2) {
                                                seconds = "0" + seconds;
                                            }
                                            temp_timer_text = minutes + ":" + seconds + ":" + decimal_places;
                                            hack_timer_text.setText(temp_timer_text);

                                            hack_timer_text.setVisibility(TextView.VISIBLE);
                                            downloadButton.setEnabled(true);
                                            downloadButton.setVisibility(ImageView.VISIBLE);
                                        }
                                    }
                                    else {
                                        if (current_hack_level < terminal_security_level[practice_level - 1]) {
                                            continueButton.setEnabled(true);
                                            continueButton.setVisibility(ImageView.VISIBLE);
                                        }
                                        else {
                                            temp_timer_text = "";
                                            if (hack_time_set == false) {
                                                temp_hack_time = ((SystemClock.elapsedRealtime() - hack_timer) / (float) 1000);
                                                true_hack_time = temp_hack_time;
                                                hack_time_set = true;
                                            }
                                            String decimal_places;
                                            if (temp_hack_time != (int) temp_hack_time) {
                                                decimal_places = (String.valueOf(temp_hack_time)).substring((String.valueOf(temp_hack_time)).indexOf(".") + 1, (String.valueOf(temp_hack_time)).indexOf(".") + 3);
                                            }
                                            else {
                                                decimal_places = "00";
                                            }
                                            temp_hack_time = (int) temp_hack_time;
                                            int minute_counter = 0;
                                            while(temp_hack_time >= 60) {
                                                temp_hack_time -= 60;
                                                minute_counter++;
                                            }
                                            String minutes = String.valueOf(minute_counter);
                                            if (minutes.length() < 2) {
                                                minutes = "0" + minutes;
                                            }
                                            String seconds = String.valueOf((int) temp_hack_time);
                                            if (seconds.length() < 2) {
                                                seconds = "0" + seconds;
                                            }
                                            temp_timer_text = minutes + ":" + seconds + ":" + decimal_places;
                                            hack_timer_text.setText(temp_timer_text);

                                            hack_timer_text.setVisibility(TextView.VISIBLE);

                                            returnButton.setEnabled(true);
                                            returnButton.setVisibility(ImageView.VISIBLE);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (bolt_positions[j] < 0) {
                        bolt_statuses[j] = 0;
                        bolt_positions[j] = 0;
                    }
                    else {
                        bolt_statuses[j] = 1;
                        bolt_positions[j] = 120;
                    }
                    final float boltX = bolt_positions[j] * (float) Math.cos((bolts[j].getRotation() + 90) * Math.PI / 180);
                    final float boltY = bolt_positions[j] * (float) Math.sin((bolts[j].getRotation() + 90) * Math.PI / 180);
                    bolts[j].setTranslationX(boltX);
                    bolts[j].setTranslationY(boltY);
                }
            }
        });
        animator.start();

    }

    public void setUiEnabled(boolean bool)
    {
        startButton.setEnabled(!bool);
        stopButton.setEnabled(bool);
        hackButton.setEnabled(bool);
        trueLockButton.setEnabled(bool);
        returnButton.setEnabled(bool);

        if (bool == true) {
            startButton.setVisibility(INVISIBLE);
            stopButton.setVisibility(View.VISIBLE);
        }
        else {
            startButton.setVisibility(View.VISIBLE);
            stopButton.setVisibility(INVISIBLE);
        }

    }

    public void saveData() {
        // Open save_file right there
        SharedPreferences save_file = getApplicationContext().getSharedPreferences("Warframe_Vaykor_Cipher_Save", 0);
        SharedPreferences.Editor savior = save_file.edit();

        if (current_terminal == 1) { //  AEDF terminal
            savior.putInt("AEDF_access_level", current_terminal_access_level);
            savior.putInt("AEDF_difficulty_level", current_terminal_difficulty_level);
        }
        else if (current_terminal == 2) { //  DDF terminal
            savior.putInt("DDF_access_level", current_terminal_access_level);
            savior.putInt("DDF_difficulty_level", current_terminal_difficulty_level);
        }
        else if (current_terminal == 3) { //  EWF terminal
            savior.putInt("EWF_access_level", current_terminal_access_level);
            savior.putInt("EWF_difficulty_level", current_terminal_difficulty_level);
        }
        else { //  IRF terminal
            savior.putInt("IRF_access_level", current_terminal_access_level);
            savior.putInt("IRF_difficulty_level", current_terminal_difficulty_level);
        }

        savior.apply();
    }


    public void Send() {
        // Sends guess at access level to Arduino
        // If Android's access level is legitimately higher than the Arduino's, Arduino's signal back
        //   will reflect that
        String output_signal = String.valueOf(current_terminal_access_level);
        output_signal.concat("\n");
        try {
            outputStream.write(output_signal.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        current_terminal_access_level = 1; // Reset the access level temporarily; reply from Arduino will update it
    }

    public void Send_Alert() {
        // Sends alert signal to Arduino; doesn't expect a signal back in Bluetooth
        String output_signal = String.valueOf("0");
        output_signal.concat("\n");
        try {
            outputStream.write(output_signal.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean BTverify(BluetoothDevice temp_device)
    {
        boolean connected=true;
        try {
            socket = temp_device.createRfcommSocketToServiceRecord(PORT_UUID);
            socket.connect();
        } catch (IOException e) {
            e.printStackTrace();
            connected=false;
        }
        if(connected)
        {
            try {
                outputStream=socket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                inputStream=socket.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }


        return connected;
    }

    public boolean BTinit()
    {
        boolean found=false;
        BluetoothAdapter bluetoothAdapter=BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            Toast.makeText(getApplicationContext(),"Device doesn't Support Bluetooth",Toast.LENGTH_SHORT).show();
        }
        if(!bluetoothAdapter.isEnabled())
        {
            Intent enableAdapter = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableAdapter, 0);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Set<BluetoothDevice> bondedDevices = bluetoothAdapter.getBondedDevices();
        if(bondedDevices.isEmpty())
        {
            Toast.makeText(getApplicationContext(),"Please Pair Security Terminal First",Toast.LENGTH_SHORT).show();
        }
        else
        {
            for (BluetoothDevice iterator : bondedDevices)
            {
                //if(iterator.getName().equals("Terminal_1"))
                String temp_address = iterator.getAddress();
                if((temp_address.equals(Terminal_1_ADDRESS) && BTverify(iterator))
                        || (temp_address.equals(Terminal_2_ADDRESS) && BTverify(iterator))
                        || (temp_address.equals(Terminal_3_ADDRESS) && BTverify(iterator))
                        || (temp_address.equals(Terminal_4_ADDRESS) && BTverify(iterator)))
                {
                    if (temp_address.equals(Terminal_1_ADDRESS)) {
                        current_terminal = 1;
                    }
                    else if (temp_address.equals(Terminal_2_ADDRESS)) {
                        current_terminal = 2;
                    }
                    else if (temp_address.equals(Terminal_3_ADDRESS)) {
                        current_terminal = 3;
                    }
                    else {
                        current_terminal = 4;
                    }
                    // Send(); // Sends data to Arduino, which will trigger a response that will set the levels for this terminal
                    device=iterator;
                    found=true;
                    codexButton.setEnabled(false);
                    titleButton.setEnabled(false);
                    practiceButton.setEnabled(false);
                    difficultySlider.setEnabled(false);
                    //Toast.makeText(getApplicationContext(),iterator.getAddress(),Toast.LENGTH_SHORT).show();
                    break;
                }
                /*Toast.makeText(getApplicationContext(),iterator.getName(),Toast.LENGTH_SHORT).show();
                device=iterator;
                found=true;
                break;*/
            }
            if (found == false) {
                Toast.makeText(getApplicationContext(),"No Paired Terminals in Range",Toast.LENGTH_SHORT).show();
            }
        }
        return found;
    }

    public boolean BTconnect()
    {
        boolean connected=true;
        try {
            socket = device.createRfcommSocketToServiceRecord(PORT_UUID);
            socket.connect();
        } catch (IOException e) {
            e.printStackTrace();
            connected=false;
        }
        if(connected)
        {
            try {
                outputStream=socket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                inputStream=socket.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }


        return connected;
    }

    public void attempt_connection() {
        if(BTinit())
        {
            /*if(BTconnect())
            {
                setUiEnabled(true);
                deviceConnected=true;
                beginListenForData();
                textView.append("\nConnection Opened!\n");
            }*/
            setUiEnabled(true);
            deviceConnected=true;
            beginListenForData();
            Send();
        }
        else {
            startButton.setEnabled(true);
            start_pressed = false;
            start_counter = 0;
        }
    }

    public void onClickStart(View view) {
        start_pressed = true;
        startButton.setEnabled(false);
    }

    void setGlobal_levels(int difficulty, int access_level) {
        ((Warframe_Vaykor_Cipher) this.getApplication()).set_terminal_difficulty_level(current_terminal - 1, difficulty);
        ((Warframe_Vaykor_Cipher) this.getApplication()).set_terminal_access_level(current_terminal - 1, access_level);
        current_terminal_difficulty_level = difficulty;
        current_terminal_access_level = access_level;

        // Go ahead and save this updated data to the shared preferences file
        saveData();
    }

    void beginListenForData()
    {
        final Handler handler = new Handler();
        stopThread = false;
        buffer = new byte[1024];
        Thread thread  = new Thread(new Runnable()
        {
            public void run()
            {
                while(!Thread.currentThread().isInterrupted() && !stopThread)
                {
                    try
                    {
                        int byteCount = inputStream.available();
                        if(byteCount > 0)
                        {
                            byte[] rawBytes = new byte[byteCount];
                            inputStream.read(rawBytes);
                            final String string=new String(rawBytes,"UTF-8");
                            //String new_string = decode_string.concat(string);
                            decode_string = decode_string.concat(string);
                            if (decode_string.contains("\n")) {
                                // Print out string twice
                                handler.post(new Runnable() {
                                    public void run()
                                    {
                                        // Printing out received data
                                        // textView.append(decode_string);
                                        // Finding the numbers for difficulty and access level
                                        int difficulty_index = decode_string.indexOf("Difficulty_Level_");
                                        int access_index = decode_string.indexOf("Access_Level_");

                                        setGlobal_levels(parseInt(decode_string.substring(difficulty_index + 17, difficulty_index + 18)),
                                                parseInt(decode_string.substring(access_index + 13, access_index + 14)));
                                        if (current_terminal_access_level >= 9) {
                                            current_terminal_access_level = 9;
                                            hackButton.setEnabled(false);
                                        }
                                        else {
                                            hackButton.setEnabled(true);
                                        }
                                        // Updating the textview
                                        textView.setText("\nConnected to ");
                                        if (current_terminal == 1) {
                                            textView.append("AEDF Data Terminal.\n");
                                        }
                                        else if (current_terminal == 2) {
                                            textView.append("DDF Data Terminal.\n");
                                        }
                                        else if (current_terminal == 3) {
                                            textView.append("EWF Data Terminal.\n");
                                        }
                                        else { //if (current_terminal == 4) {
                                            textView.append("IRF Data Terminal.\n");
                                        }
                                        textView.append ("\nEncryption Level: ");
                                        textView.append(String.valueOf(current_terminal_difficulty_level));
                                        textView.append("\n");

                                        if (current_terminal_access_level >= 9) {
                                            textView.append("\nAll 8 files recovered.\n");
                                        }
                                        else {
                                            textView.append ("\n");
                                            textView.append(String.valueOf(current_terminal_access_level - 1));
                                            textView.append(" / 8 files accessed.\n");
                                        }
                                        decode_string = ""; // Reset decoder

                                        // Resetting the alert symbols
                                        for (int k = 0; k < 4; k++) {
                                            open_marks[k].setVisibility(ImageView.INVISIBLE);
                                        }
                                    }
                                });
                            }

                        }
                    }
                    catch (IOException ex)
                    {
                        stopThread = true;
                    }
                }
            }
        });

        thread.start();
    }

    public void onClickStop(View view) throws IOException {
        stopThread = true;
        outputStream.close();
        inputStream.close();
        socket.close();
        setUiEnabled(false);
        deviceConnected=false;
        codexButton.setEnabled(true);
        titleButton.setEnabled(true);
        practiceButton.setEnabled(true);
        difficultySlider.setEnabled(true);
        textView.setText("\nNot connected to any data terminals.\n");
    }

    public void onClickClear(View view) {
        textView.setText("");
    }

    public void update_Alerts() {
        // Only update the alerts governed by the difficulty level (affected by practicing)
        if (practicing == false) {
            for (int i = 0; i < 5 - current_terminal_difficulty_level; i++) {
                open_marks[i].setVisibility(ImageView.VISIBLE);
                if (alert_counter >= i + 1) {
                    filled_marks[i].setVisibility(ImageView.VISIBLE);
                    filled_marks[i].setAlpha(1f); // Resetting transparency
                } else {
                    filled_marks[i].setVisibility(INVISIBLE);
                }
            }
        }
        else {
            for (int i = 0; i < 5 - practice_level; i++) {
                open_marks[i].setVisibility(ImageView.VISIBLE);
                if (alert_counter >= i + 1) {
                    filled_marks[i].setVisibility(ImageView.VISIBLE);
                    filled_marks[i].setAlpha(1f); // Resetting transparency
                } else {
                    filled_marks[i].setVisibility(INVISIBLE);
                }
            }
        }
    }

    public void setBolt_Pattern () {
        int miss_counter = 0;
        int hit_counter = 0;
        progress_degrees = (float) dice.nextInt(100);
        if (dice.nextInt(2) == 0) {
            pick_direction = 1;
        }
        else {
            pick_direction = -1;
        }
        for (int u = 0; u < 8; u++) {
            bolt_activations[u] = false;
            bolt_positions[u] = 0;
            bolt_directions[u] = -1;
            bolts[u].setVisibility(INVISIBLE);
            int temp_difficulty_level;
            if (practicing == false) {
                temp_difficulty_level = current_terminal_difficulty_level;
            }
            else {
                temp_difficulty_level = practice_level;
            }

            if (temp_difficulty_level <= 1) {
                int randomized = dice.nextInt(8); // Randomizing 6/8 of the bolts
                if (randomized < 6 && hit_counter < 6 || miss_counter > 1) {
                    bolt_activations[u] = true;
                    bolts[u].setVisibility(ImageView.VISIBLE);
                    hit_counter++;
                }
                else {
                    miss_counter++;
                }
                speed_multiplier = 3;

            }
            else if (temp_difficulty_level <= 2) {
                int randomized = dice.nextInt(8); // Randomizing 6/8 of the bolts
                if (randomized < 6  && hit_counter < 6 || miss_counter > 1) {
                    bolt_activations[u] = true;
                    bolts[u].setVisibility(ImageView.VISIBLE);
                    hit_counter++;
                }
                else {
                    miss_counter++;
                }
                speed_multiplier = (float) 3.5;
            }
            else if (temp_difficulty_level <= 3) {
                // Level 3 and 4 terminals use all 8 bolts
                bolt_activations[u] = true;
                bolts[u].setVisibility(ImageView.VISIBLE);

                speed_multiplier = 4;
            }
            else {
                bolt_activations[u] = true;
                bolts[u].setVisibility(ImageView.VISIBLE);
                speed_multiplier = (float) 4.5;
            }
        }
    }


    public void clear_Lock_Icons() {
        for (int i = 0; i < 6; i++) { // reset all icons
            locks[i].setVisibility(INVISIBLE);
            unlocks[i].setVisibility(INVISIBLE);
        }
    }
    public void set_Lock_Icons() {
        clear_Lock_Icons();
        if (practicing == false) {
            for (int j = 0; j < terminal_security_level[current_terminal_difficulty_level - 1]; j++) { // show possible locks
                locks[j].setVisibility(ImageView.VISIBLE);
            }
        }
        else {
            for (int j = 0; j < terminal_security_level[practice_level - 1]; j++) { // show possible locks
                locks[j].setVisibility(ImageView.VISIBLE);
            }
        }
    }

    public void reset_Lock() {
        setBolt_Pattern();
        trueLockButton.setEnabled(true);
        trueLockButton.setVisibility(ImageView.VISIBLE);
        continueButton.setEnabled(false);
        continueButton.setVisibility(INVISIBLE);
        hack_instance_completed = false;
        alert_counter = 0;
        update_Alerts();
    }

    public void proceed_to_next_Lock(View view) {
        reset_Lock();
    }

    public void start_Hacking(View view) {
        stopButton.setEnabled(false);
        hackButton.setEnabled(false);

        stopButton.setVisibility(INVISIBLE);
        hackButton.setVisibility(INVISIBLE);
        textView.setVisibility(INVISIBLE);
        codexButton.setVisibility(INVISIBLE);
        titleButton.setVisibility(INVISIBLE);
        practiceButton.setVisibility(INVISIBLE);
        difficultySlider.setVisibility(INVISIBLE);

        trueLockButton.setEnabled(true);
        trueLockButton.setVisibility(View.VISIBLE);

        pick.setVisibility(ImageView.VISIBLE);
        lock.setVisibility(ImageView.VISIBLE);

        setBolt_Pattern(); // Sets Bolts
        set_Lock_Icons();
        alert_counter = 0;
        update_Alerts();

        hack_timer = SystemClock.elapsedRealtime();
    }

    public void start_Download(View view) {
        downloading = true;
        download_progress = 0;
        downloadButton.setEnabled(false);
        downloadButton.setVisibility(INVISIBLE);
        downloadBar.setVisibility(ProgressBar.VISIBLE);
    }

    public void finish_Hacking(View view) {
        // Open save_file right there
        SharedPreferences save_file = getApplicationContext().getSharedPreferences("Warframe_Vaykor_Cipher_Save", 0);
        SharedPreferences.Editor savior = save_file.edit();

        if (!practicing) {
            stopButton.setEnabled(true);
            stopButton.setVisibility(View.VISIBLE);
            hackButton.setEnabled(true);
            current_terminal_access_level++; // Adds one level to current access level
            Send(); // Try to update access level
            download_progress = 0;
            downloading = false;


            String terminal_name;
            if (current_terminal == 1) {
                terminal_name = "AEDF";
            }
            else if (current_terminal == 2) {
                terminal_name = "DDF";
            }
            else if (current_terminal == 3) {
                terminal_name = "EWF";
            }
            else {
                terminal_name = "IRF";
            }

            if (current_terminal_difficulty_level == 1 && true_hack_time < save_file.getFloat("Real_Level_1_number", 1000000000)) {
                savior.putFloat("Real_Level_1_number", (float) true_hack_time);
                savior.putString("Real_Level_1_text", terminal_name + " Level 1 Cipher: " + temp_timer_text + " on " + DateFormat.getDateTimeInstance().format(new Date()));
            }
            else if (current_terminal_difficulty_level == 2 && true_hack_time < save_file.getFloat("Real_Level_2_number", 1000000000)) {
                savior.putFloat("Real_Level_2_number", (float) true_hack_time);
                savior.putString("Real_Level_2_text", "Level 2 Cipher: " + temp_timer_text + " on " + DateFormat.getDateTimeInstance().format(new Date()));
            }
            else if (current_terminal_difficulty_level == 3 && true_hack_time < save_file.getFloat("Real_Level_3_number", 1000000000)) {
                savior.putFloat("Real_Level_3_number", (float) true_hack_time);
                savior.putString("Real_Level_3_text", "Level 3 Cipher: " + temp_timer_text + " on " + DateFormat.getDateTimeInstance().format(new Date()));
            }
            else if (current_terminal_difficulty_level == 4 && true_hack_time < save_file.getFloat("Real_Level_4_number", 1000000000)) {
                savior.putFloat("Real_Level_4_number", (float) true_hack_time);
                savior.putString("Real_Level_4_text", "Level 4 Cipher: " + temp_timer_text + " on " + DateFormat.getDateTimeInstance().format(new Date()));
            }


        }
        else {
            startButton.setEnabled(true);
            startButton.setVisibility(View.VISIBLE);

            if (practice_level == 1 && true_hack_time < save_file.getFloat("Practice_Level_1_number", 1000000000)) {
                savior.putFloat("Practice_Level_1_number", (float) true_hack_time);
                savior.putString("Practice_Level_1_text", "Level 1 Cipher, Practice: " + temp_timer_text + " on " + DateFormat.getDateTimeInstance().format(new Date()));
            }
            else if (practice_level == 2 && true_hack_time < save_file.getFloat("Practice_Level_2_number", 1000000000)) {
                savior.putFloat("Practice_Level_2_number", (float) true_hack_time);
                savior.putString("Practice_Level_2_text", "Level 2 Cipher, Practice: " + temp_timer_text + " on " + DateFormat.getDateTimeInstance().format(new Date()));
            }
            else if (practice_level == 3 && true_hack_time < save_file.getFloat("Practice_Level_3_number", 1000000000)) {
                savior.putFloat("Practice_Level_3_number", (float) true_hack_time);
                savior.putString("Practice_Level_3_text", "Level 3 Cipher, Practice: " + temp_timer_text + " on " + DateFormat.getDateTimeInstance().format(new Date()));
            }
            else if (practice_level == 4 && true_hack_time < save_file.getFloat("Practice_Level_4_number", 1000000000)) {
                savior.putFloat("Practice_Level_4_number", (float) true_hack_time);
                savior.putString("Practice_Level_4_text", "Level 4 Cipher, Practice: " + temp_timer_text + " on " + DateFormat.getDateTimeInstance().format(new Date()));
            }

        }

        savior.apply();

        hackButton.setVisibility(View.VISIBLE);
        textView.setVisibility(View.VISIBLE);
        codexButton.setVisibility(View.VISIBLE);
        titleButton.setVisibility(View.VISIBLE);
        practiceButton.setVisibility(View.VISIBLE);
        difficultySlider.setVisibility(SeekBar.VISIBLE);


        trueLockButton.setEnabled(false);
        trueLockButton.setVisibility(INVISIBLE);
        returnButton.setEnabled(false);
        returnButton.setVisibility(INVISIBLE);

        pick.setVisibility(INVISIBLE);
        lock.setVisibility(INVISIBLE);
        for (int u = 0; u < 8; u++) {
            bolts[u].setVisibility(INVISIBLE);
            bolt_positions[u] = 0;
            bolt_directions[u] = -1;
        }
        if (practicing == false) {
            for (int i = 0; i < 5 - current_terminal_difficulty_level; i++) {
                open_marks[i].setVisibility(INVISIBLE);
                filled_marks[i].setVisibility(INVISIBLE);
            }
        }
        else {
            for (int i = 0; i < 5 - practice_level; i++) {
                open_marks[i].setVisibility(INVISIBLE);
                filled_marks[i].setVisibility(INVISIBLE);
            }
        }
        clear_Lock_Icons();
        hack_instance_completed = false;
        current_hack_level = 0;

        alert_counter = 0;
        practicing = false;
        hack_time_set = false;
        hack_timer_text.setVisibility(TextView.INVISIBLE);

        Toast.makeText(getApplicationContext(),"Most recent hack time: " + temp_timer_text,Toast.LENGTH_SHORT).show();
    }

    public void Practice(View view) {
        startButton.setEnabled(false);
        startButton.setVisibility(INVISIBLE);
        practicing = true;
        practice_level = 1 + difficultySlider.getProgress();
        start_Hacking(view);
    }

    public void Lock_or_Unlock(View view) {
        for (int k = 0; k < 8; k++) {
            float conversion_degrees = (float) 0.25 + progress_degrees;
            if (conversion_degrees > 1) {
                conversion_degrees -= (float) 1;
            }
            else if (conversion_degrees < 0) {
                conversion_degrees += (float) 1;
            }

            if ((conversion_degrees >= 0.125 * (float) k) && (conversion_degrees <= 0.125 + 0.125 *
                    (float) k) && (bolt_activations[k])) { // Only locking bolts that are active and in the right zone
                bolt_directions[k] = -1 * bolt_directions[k];
                if (bolt_statuses[k] == 1) {
                    alert_counter++; // Sets off one alert
                    decay_counter = SystemClock.elapsedRealtime();
                    alert_alpha = 1;
                    if (!practicing) {
                        if (alert_counter <= 5 - current_terminal_difficulty_level) {
                            update_Alerts();
                        }
                        else {
                            reset_Lock();
                            // Also signal the Arduino to set off the alarm
                            Send_Alert();
                        }
                    }
                    else {
                        if (alert_counter <= 5 - practice_level) {
                            update_Alerts();
                        }
                        else {
                            reset_Lock();
                        }
                    }
                }
                if (practicing == false) {
                    if (current_terminal_difficulty_level == 2 || current_terminal_difficulty_level == 3) {
                        pick_direction *= (float) -1;
                    } else if (current_terminal_difficulty_level == 4) {
                        if (dice.nextInt(2) == 0) {
                            pick_direction *= (float) -1;
                        }
                    }
                }
                else {
                    if (practice_level == 2 ||
                            practice_level == 3) {
                        pick_direction *= (float) -1;
                    } else if (practice_level == 4) {
                        if (dice.nextInt(2) == 0) {
                            pick_direction *= (float) -1;
                        }
                    }
                }
                break;
            }
        }
    }

    public void go_to_Codex(View view) throws IOException {
        Intent intent = new Intent(this, Codex.class);
        startActivity(intent);
    }

    public void go_to_Title(View view) throws IOException {
        Intent intent = new Intent(this, Title_Screen.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
    }

}
