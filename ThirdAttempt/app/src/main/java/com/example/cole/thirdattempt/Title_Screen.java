package com.example.cole.thirdattempt;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Handler;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;
import android.view.MotionEvent;
import android.view.View;

public class Title_Screen extends AppCompatActivity {

    public static final String PREFS_NAME = "Warframe_Vaykor_Cipher_Save";

    Button statisticsButton, terminalButton, backButton, codexButton, creditsButton;
    TextView greetingText, mainText, miniTitletext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title__screen);


        statisticsButton = (Button) findViewById(R.id.buttonStatistics);
        terminalButton = (Button) findViewById(R.id.buttonTerminal);
        creditsButton = (Button) findViewById(R.id.buttonCredits);
        codexButton = (Button) findViewById(R.id.buttonCodex);
        backButton = (Button) findViewById(R.id.buttonBack);

        greetingText = (TextView) findViewById(R.id.textView);
        mainText = (TextView) findViewById(R.id.textStats_and_Credits);
        miniTitletext = (TextView) findViewById(R.id.textminiTitles);

        mainText.setMovementMethod(new ScrollingMovementMethod());


        // Have to actually put the shared preferences declaration in the same general scope as its usage
        SharedPreferences save_file = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor savior = save_file.edit();

        // Initial Setup: First, always commit editor to make sure that the file actually exists
        savior.putInt("File_Created", 1);
        savior.commit();
        // Now that we're sure that the file exists, checks if file contains "File_Populated"
        // If Yes, do nothing
        // If No, populate file with default values
        if (!(save_file.contains("File_Populated"))) {
            savior.putInt("File_Populated", 1);
            //  AEDF terminal
            savior.putInt("AEDF_access_level", 1);
            savior.putInt("AEDF_difficulty_level", 1);
            //  DDF terminal
            savior.putInt("DDF_access_level", 1);
            savior.putInt("DDF_difficulty_level", 1);
            //  EWF terminal
            savior.putInt("EWF_access_level", 1);
            savior.putInt("EWF_difficulty_level", 1);
            //  IRF terminal
            savior.putInt("IRF_access_level", 1);
            savior.putInt("IRF_difficulty_level", 1);

            // Can't forget to actually save the changes XD
            savior.apply();
        }


        // Retrieving Terminal_1 (AEDF) data
        ((Warframe_Vaykor_Cipher) this.getApplication()).set_terminal_access_level(0, save_file.getInt("AEDF_access_level", 0));
        ((Warframe_Vaykor_Cipher) this.getApplication()).set_terminal_difficulty_level(0, save_file.getInt("AEDF_difficulty_level", 0));
        // Retrieving Terminal_2 (DDF) data
        ((Warframe_Vaykor_Cipher) this.getApplication()).set_terminal_access_level(1, save_file.getInt("DDF_access_level", 0));
        ((Warframe_Vaykor_Cipher) this.getApplication()).set_terminal_difficulty_level(1, save_file.getInt("DDF_difficulty_level", 0));
        // Retrieving Terminal_3 (EWF) data
        ((Warframe_Vaykor_Cipher) this.getApplication()).set_terminal_access_level(2, save_file.getInt("EWF_access_level", 0));
        ((Warframe_Vaykor_Cipher) this.getApplication()).set_terminal_difficulty_level(2, save_file.getInt("EWF_difficulty_level", 0));
        // Retrieving Terminal_4 (IRF) data
        ((Warframe_Vaykor_Cipher) this.getApplication()).set_terminal_access_level(3, save_file.getInt("IRF_access_level", 0));
        ((Warframe_Vaykor_Cipher) this.getApplication()).set_terminal_difficulty_level(3, save_file.getInt("IRF_difficulty_level", 0));
    }


    public void go_to_Codex(View view) {
        Intent intent = new Intent(this, Codex.class);
        startActivity(intent);
    }

    public void go_to_Terminal(View view) {
        Intent intent = new Intent(this, TerminalActivity.class);
        startActivity(intent);
    }


    public void go_to_Statistics(View view) {
        codexButton.setEnabled(false);
        codexButton.setVisibility(Button.INVISIBLE);
        statisticsButton.setEnabled(false);
        statisticsButton.setVisibility(Button.INVISIBLE);
        creditsButton.setEnabled(false);
        creditsButton.setVisibility(Button.INVISIBLE);
        terminalButton.setEnabled(false);
        terminalButton.setVisibility(Button.INVISIBLE);
        backButton.setEnabled(true);
        backButton.setVisibility(Button.VISIBLE);

        greetingText.setVisibility(TextView.INVISIBLE);
        miniTitletext.setText("Statistics");
        miniTitletext.setVisibility(TextView.VISIBLE);


        // Have to actually put the shared preferences declaration in the same general scope as its usage
        SharedPreferences save_file = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor savior = save_file.edit();


        mainText.setText("Fastest Hack Times: \n\n\n" +
                save_file.getString("Practice_Level_1_text", "Level 1 Cipher, Practice: N/A") + "\n\n" +
                save_file.getString("Practice_Level_2_text", "Level 2 Cipher, Practice: N/A") + "\n\n" +
                save_file.getString("Practice_Level_3_text", "Level 3 Cipher, Practice: N/A") + "\n\n" +
                save_file.getString("Practice_Level_4_text", "Level 4 Cipher, Practice: N/A") + "\n\n\n\n" +
                save_file.getString("Real_Level_1_text", "Real Level 1 Cipher: N/A") + "\n\n" +
                save_file.getString("Real_Level_2_text", "Real Level 2 Cipher: N/A") + "\n\n" +
                save_file.getString("Real_Level_3_text", "Real Level 3 Cipher: N/A") + "\n\n" +
                save_file.getString("Real_Level_4_text", "Real Level 4 Cipher: N/A"));

        mainText.setVisibility(TextView.VISIBLE);
        mainText.setEnabled(true);
    }


    public void go_to_Credits(View view) {
        codexButton.setEnabled(false);
        codexButton.setVisibility(Button.INVISIBLE);
        statisticsButton.setEnabled(false);
        statisticsButton.setVisibility(Button.INVISIBLE);
        creditsButton.setEnabled(false);
        creditsButton.setVisibility(Button.INVISIBLE);
        terminalButton.setEnabled(false);
        terminalButton.setVisibility(Button.INVISIBLE);
        backButton.setEnabled(true);
        backButton.setVisibility(Button.VISIBLE);

        greetingText.setVisibility(TextView.INVISIBLE);
        miniTitletext.setText("Credits");
        miniTitletext.setVisibility(TextView.VISIBLE);

        // Have to actually put the shared preferences declaration in the same general scope as its usage
        SharedPreferences save_file = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor savior = save_file.edit();

        mainText.setText("Code: Cole Allen \n\n" +
                        "Graphical Design (for what it's worth): Cole Allen \n\n" +
                        "Story: Cole Allen \n\n" +
                        "Briefing Files Editor: Moriah Bischann \n\n" +
                        "Mulan References (and all other references): Cole Allen \n\n\n\n" +
                        "Inspiration for app: Digital Extremes' WARFRAME. No copryright or trademark infringment is intended. \n\n\n\n" +
                        "Special thanks for project oversight goes to:\n\n" +
                        "Donnie Pinkston\n\n\n\n" +
                        "Special thanks for debugging help goes to:\n\n" +
                        "Justin Leong\n\n\n\n" +
                        "Special thanks for Android/Bluetooth/Arduino help goes to:\n\n" +
                        "Hariharan Mathavan via his excellent guides on allaboutcircuits.com\n\n" +
                        "stackoverflow.com just in general for existing\n\n\n\n" +
                        "Special thanks for graphics goes to:\n\n" +
                        "GIMP for being a great free graphics design program\n\n\n\n" +
                        "A really big THANK YOU for playtesting goes to: \n\n" +
                        "All those poor unfortunate souls at Caltech whom I strongarmed into 'trying' this app. I'm very grateful for their patience and feedback, the app would have been much worse without it.\n\n\n\n" +
                        "And finally:\n\n" +
                        "Thanks be to God for everything, including but certainly not limited to:\n\n" +
                        "   Being able to finish all 136 codex entries in time for Ditch Day 2017 and in semi-coherent fashion despite extreme sleep deprivation\n\n" +
                        "   The app working almost exactly as intended on Ditch Day 2017\n\n" +
                        "   Dropbox's servers not going down on Ditch Day 2017\n\n" +
                        "   Everyone that kept me sane over the last term");



        mainText.setVisibility(TextView.VISIBLE);
        mainText.setEnabled(true);
    }


    public void go_back(View view) {
        codexButton.setEnabled(true);
        codexButton.setVisibility(Button.VISIBLE);
        statisticsButton.setEnabled(true);
        statisticsButton.setVisibility(Button.VISIBLE);
        creditsButton.setEnabled(true);
        creditsButton.setVisibility(Button.VISIBLE);
        terminalButton.setEnabled(true);
        terminalButton.setVisibility(Button.VISIBLE);
        backButton.setEnabled(false);
        backButton.setVisibility(Button.INVISIBLE);

        greetingText.setVisibility(TextView.VISIBLE);
        miniTitletext.setVisibility(TextView.INVISIBLE);
        mainText.setVisibility(TextView.INVISIBLE);
        mainText.setEnabled(false);

    }

    @Override
    public void onBackPressed() {
    }
}
