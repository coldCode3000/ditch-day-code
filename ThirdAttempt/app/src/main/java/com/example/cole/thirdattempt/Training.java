package com.example.cole.thirdattempt;



import android.animation.ValueAnimator;
import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Handler;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;
import java.util.Set;
import java.util.UUID;


public class Training extends AppCompatActivity {

    int current_terminal = 1;
    Button hackButton, lockButton, returnButton, continueButton, titleButton;
    TextView textView;
    ImageView pick, lock;

    float speed_multiplier = 1;
    Random dice = new Random();
    int[] bolt_statuses;    // Whether bolt has been pushed
    int[] bolt_directions;    // Adding or subtracting from bolt position
    boolean[] bolt_activations;    // Enabling bolts
    float[] bolt_positions; // Position of bolt relative to starting position
    ImageView[] bolts;

    int alert_counter; // Keeps track of how many alerts have been set off
    ImageView[] open_marks;
    ImageView[] filled_marks;


    int[] terminal_security_level = new int[4];
    int current_hack_level = 0;
    ImageView[] locks;
    ImageView[] unlocks;
    boolean hack_instance_completed = false;

    int current_terminal_difficulty_level = 1;

    float progress_degrees, pick_direction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terminal);
        hackButton = (Button) findViewById(R.id.buttonHack);
        textView = (TextView) findViewById(R.id.textView);
        lockButton = (Button) findViewById(R.id.buttonLock);
        continueButton = (Button) findViewById(R.id.buttonContinue);
        returnButton = (Button) findViewById(R.id.buttonReturn);
        titleButton = (Button) findViewById(R.id.buttonTitle);

        setUiEnabled(true);


        pick = (ImageView) findViewById(R.id.hacking_pick);
        lock = (ImageView) findViewById(R.id.hacking_lock);

        pick_direction = 1;

        bolt_statuses = new int[8];
        bolt_directions = new int[8];
        bolt_activations = new boolean[8];
        bolt_positions = new float[8];
        bolts = new ImageView[8];

        open_marks = new ImageView[4];
        filled_marks = new ImageView[4];


        locks = new ImageView[6];
        unlocks = new ImageView[6];

        bolts[0] = (ImageView) findViewById(R.id.tumbler_1);
        bolts[1] = (ImageView) findViewById(R.id.tumbler_2);
        bolts[2] = (ImageView) findViewById(R.id.tumbler_3);
        bolts[3] = (ImageView) findViewById(R.id.tumbler_4);
        bolts[4] = (ImageView) findViewById(R.id.tumbler_5);
        bolts[5] = (ImageView) findViewById(R.id.tumbler_6);
        bolts[6] = (ImageView) findViewById(R.id.tumbler_7);
        bolts[7] = (ImageView) findViewById(R.id.tumbler_8);
        for (int i = 0; i < 8; i++) {
            bolt_statuses[i] = 0;
            bolt_directions[i] = -1;
            bolt_activations[i] = true;
            bolt_positions[i] = 0;
        }

        alert_counter = 0;
        open_marks[0] = (ImageView) findViewById(R.id.open_mark_1);
        open_marks[1] = (ImageView) findViewById(R.id.open_mark_2);
        open_marks[2] = (ImageView) findViewById(R.id.open_mark_3);
        open_marks[3] = (ImageView) findViewById(R.id.open_mark_4);
        filled_marks[0] = (ImageView) findViewById(R.id.filled_mark_1);
        filled_marks[1] = (ImageView) findViewById(R.id.filled_mark_2);
        filled_marks[2] = (ImageView) findViewById(R.id.filled_mark_3);
        filled_marks[3] = (ImageView) findViewById(R.id.filled_mark_4);

        terminal_security_level[0] = 3;
        terminal_security_level[1] = 4;
        terminal_security_level[2] = 5;
        terminal_security_level[3] = 6;

        locks[0] = (ImageView) findViewById(R.id.iconLock_1);
        locks[1] = (ImageView) findViewById(R.id.iconLock_2);
        locks[2] = (ImageView) findViewById(R.id.iconLock_3);
        locks[3] = (ImageView) findViewById(R.id.iconLock_4);
        locks[4] = (ImageView) findViewById(R.id.iconLock_5);
        locks[5] = (ImageView) findViewById(R.id.iconLock_6);

        unlocks[0] = (ImageView) findViewById(R.id.iconUnlock_1);
        unlocks[1] = (ImageView) findViewById(R.id.iconUnlock_2);
        unlocks[2] = (ImageView) findViewById(R.id.iconUnlock_3);
        unlocks[3] = (ImageView) findViewById(R.id.IconUnlock_4);
        unlocks[4] = (ImageView) findViewById(R.id.iconUnlock_5);
        unlocks[5] = (ImageView) findViewById(R.id.iconUnlock_6);

        final ValueAnimator animator = ValueAnimator.ofFloat(0.0f, 1.0f);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setInterpolator(new LinearInterpolator());
        animator.setDuration(10000L);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // progress_degrees = (float) animation.getAnimatedValue();
                progress_degrees += pick_direction * 0.004 * speed_multiplier;
                if (progress_degrees > 1) {
                    progress_degrees -= (float) 1;
                }
                else if (progress_degrees < 0) {
                    progress_degrees += (float) 1;
                }
                final float radius = lock.getWidth() / 2;
                final float translationX = radius * (float) (Math.cos(2 * Math.PI * progress_degrees));
                final float translationY = radius * (float) (Math.sin(2 * Math.PI * progress_degrees));
                pick.setTranslationX(translationX);
                pick.setTranslationY(translationY);
                pick.setRotation((float) (90 + 360.0 * progress_degrees));

                // Hijacking this animation to also alter the bolt positions
                for (int j = 0; j < 8; j++) {

                    if (bolt_positions[j] >= 0 && bolt_positions[j] <= 120){
                        bolt_positions[j] += 5 * (float) bolt_directions[j];
                        if (bolt_positions[j] < 0) {
                            bolt_statuses[j] = 0;
                            bolt_positions[j] = 0;
                        }
                        else if (bolt_positions[j] > 120){
                            bolt_statuses[j] = 1;
                            bolt_positions[j] = 120;
                            if (hack_instance_completed == false) {
                                hack_instance_completed = true;
                                for (int i = 0; i < 8; i++) {
                                    if (bolt_statuses[i] != 1 && bolt_activations[i]) {
                                        hack_instance_completed = false;
                                    }
                                }
                                if (hack_instance_completed) {
                                    // Should verify with Arduino here.
                                    current_hack_level++;

                                    lockButton.setEnabled(false);
                                    lockButton.setVisibility(ImageView.INVISIBLE);
                                    for (int k = 0; k < current_hack_level; k++) {
                                        locks[k].setVisibility(ImageView.INVISIBLE);
                                        unlocks[k].setVisibility(ImageView.VISIBLE);
                                    }
                                    if (current_hack_level < terminal_security_level[current_terminal_difficulty_level - 1]) {
                                        continueButton.setEnabled(true);
                                        continueButton.setVisibility(ImageView.VISIBLE);
                                    } else {
                                        returnButton.setEnabled(true);
                                        returnButton.setVisibility(ImageView.VISIBLE);
                                    }
                                }
                            }
                        }
                    }
                    else if (bolt_positions[j] < 0) {
                        bolt_statuses[j] = 0;
                        bolt_positions[j] = 0;
                    }
                    else {
                        bolt_statuses[j] = 1;
                        bolt_positions[j] = 120;
                    }
                    final float boltX = bolt_positions[j] * (float) Math.cos((bolts[j].getRotation() + 90) * Math.PI / 180);
                    final float boltY = bolt_positions[j] * (float) Math.sin((bolts[j].getRotation() + 90) * Math.PI / 180);
                    bolts[j].setTranslationX(boltX);
                    bolts[j].setTranslationY(boltY);
                }
            }
        });
        animator.start();

    }

    public void setUiEnabled(boolean bool)
    {
        hackButton.setEnabled(bool);
        textView.setEnabled(bool);
        lockButton.setEnabled(bool);
        returnButton.setEnabled(bool);
        continueButton.setEnabled(bool);

        if (bool == true) {
            hackButton.setVisibility(View.VISIBLE);
        }
        else {
            hackButton.setVisibility(View.INVISIBLE);
        }

    }

    public void update_Alerts() {
        // Only update the alerts governed by the difficulty level
        for (int i = 0; i < 5 -
                ((Warframe_Vaykor_Cipher) this.getApplication()).get_terminal_difficulty_level(current_terminal - 1);
             i++) {
            open_marks[i].setVisibility(ImageView.VISIBLE);
            if (alert_counter >= i + 1) {
                filled_marks[i].setVisibility(ImageView.VISIBLE);
            }
            else {
                filled_marks[i].setVisibility(ImageView.INVISIBLE);
            }
        }
    }

    public void setBolt_Pattern () {
        int miss_counter = 0;
        int hit_counter = 0;
        progress_degrees = (float) dice.nextInt(100);
        if (dice.nextInt(2) == 0) {
            pick_direction = 1;
        }
        else {
            pick_direction = -1;
        }
        for (int u = 0; u < 8; u++) {
            bolt_activations[u] = false;
            bolt_positions[u] = 0;
            bolt_directions[u] = -1;
            bolts[u].setVisibility(ImageView.INVISIBLE);
            if (((Warframe_Vaykor_Cipher) this.getApplication()).get_terminal_difficulty_level(current_terminal - 1) <= 1) {
                int randomized = dice.nextInt(8); // Randomizing 6/8 of the bolts
                if (randomized < 6 && hit_counter < 6 || miss_counter > 1) {
                    bolt_activations[u] = true;
                    bolts[u].setVisibility(ImageView.VISIBLE);
                    hit_counter++;
                }
                else {
                    miss_counter++;
                }
                speed_multiplier = 1;

            }
            else if (((Warframe_Vaykor_Cipher) this.getApplication()).get_terminal_difficulty_level(current_terminal - 1) <= 2) {
                int randomized = dice.nextInt(8); // Randomizing 6/8 of the bolts
                if (randomized < 6  && hit_counter < 6 || miss_counter > 1) {
                    bolt_activations[u] = true;
                    bolts[u].setVisibility(ImageView.VISIBLE);
                    hit_counter++;
                }
                else {
                    miss_counter++;
                }
                speed_multiplier = 2;
            }
            else if (((Warframe_Vaykor_Cipher) this.getApplication()).get_terminal_difficulty_level(current_terminal - 1) <= 3) {
                // Level 3 and 4 terminals use all 8 bolts
                bolt_activations[u] = true;
                bolts[u].setVisibility(ImageView.VISIBLE);

                speed_multiplier = 3;
            }
            else {
                bolt_activations[u] = true;
                bolts[u].setVisibility(ImageView.VISIBLE);
                speed_multiplier = 4;
            }
        }
    }


    public void clear_Lock_Icons() {
        for (int i = 0; i < 6; i++) { // reset all icons
            locks[i].setVisibility(ImageView.INVISIBLE);
            unlocks[i].setVisibility(ImageView.INVISIBLE);
        }
    }
    public void set_Lock_Icons() {
        clear_Lock_Icons();
        for (int j = 0; j < terminal_security_level[current_terminal_difficulty_level - 1]; j++) { // show possible locks
            locks[j].setVisibility(ImageView.VISIBLE);
        }
    }

    public void proceed_to_next_Lock(View view) {
        setBolt_Pattern();
        lockButton.setEnabled(true);
        lockButton.setVisibility(ImageView.VISIBLE);
        continueButton.setEnabled(false);
        continueButton.setVisibility(ImageView.INVISIBLE);
        hack_instance_completed = false;
        alert_counter = 0;
        update_Alerts();
    }

    public void start_Hacking(View view) {
        hackButton.setEnabled(false);
        textView.setEnabled(false);

        hackButton.setVisibility(View.INVISIBLE);
        textView.setVisibility(View.INVISIBLE);
        titleButton.setVisibility(View.INVISIBLE);

        lockButton.setEnabled(true);
        lockButton.setVisibility(View.VISIBLE);

        pick.setVisibility(ImageView.VISIBLE);
        lock.setVisibility(ImageView.VISIBLE);

        setBolt_Pattern(); // Sets Bolts
        set_Lock_Icons();
        alert_counter = 0;
        update_Alerts();
    }

    public void finish_Hacking(View view) {
        hackButton.setEnabled(true);
        textView.setEnabled(true);

        hackButton.setVisibility(View.VISIBLE);
        textView.setVisibility(View.VISIBLE);
        titleButton.setVisibility(View.VISIBLE);

        lockButton.setEnabled(false);
        lockButton.setVisibility(View.INVISIBLE);
        returnButton.setEnabled(false);
        returnButton.setVisibility(View.INVISIBLE);

        pick.setVisibility(ImageView.INVISIBLE);
        lock.setVisibility(ImageView.INVISIBLE);
        for (int u = 0; u < 8; u++) {
            bolts[u].setVisibility(ImageView.INVISIBLE);
            bolt_positions[u] = 0;
            bolt_directions[u] = -1;
        }
        for (int i = 0; i < 5 -
                ((Warframe_Vaykor_Cipher) this.getApplication()).get_terminal_difficulty_level(current_terminal - 1);
             i++) {
            open_marks[i].setVisibility(ImageView.INVISIBLE);
            filled_marks[i].setVisibility(ImageView.INVISIBLE);
        }
        clear_Lock_Icons();
        hack_instance_completed = false;
        current_hack_level = 0;

        alert_counter = 0;
    }


    public void Lock_or_Unlock(View view) {
        for (int k = 0; k < 8; k++) {
            float conversion_degrees = (float) 0.25 + progress_degrees;
            if (conversion_degrees > 1) {
                conversion_degrees -= (float) 1;
            }
            else if (conversion_degrees < 0) {
                conversion_degrees += (float) 1;
            }

            if ((conversion_degrees >= 0.125 * (float) k) && (conversion_degrees <= 0.125 + 0.125 *
                    (float) k) && (bolt_activations[k])) { // Only locking bolts that are active and in the right zone
                bolt_directions[k] = -1 * bolt_directions[k];
                if (bolt_statuses[k] == 1) {
                    alert_counter++; // Sets off one alert
                    update_Alerts();
                }
                if (((Warframe_Vaykor_Cipher) this.getApplication()).get_terminal_difficulty_level(current_terminal - 1) == 2 ||
                        ((Warframe_Vaykor_Cipher) this.getApplication()).get_terminal_difficulty_level(current_terminal - 1) == 3) {
                    pick_direction *= (float) -1;
                }
                else if (((Warframe_Vaykor_Cipher) this.getApplication()).get_terminal_difficulty_level(current_terminal - 1) == 4) {
                    if (dice.nextInt(2) == 0) {
                        pick_direction *= (float) -1;
                    }
                }
                break;
            }
        }
    }

    public void go_to_Title(View view) throws IOException {
        Intent intent = new Intent(this, Title_Screen.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
    }

}
