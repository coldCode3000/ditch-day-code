package com.example.cole.thirdattempt;

import android.app.Application;

/**
 * Created by Cole on 4/27/2017.
 */

public class Warframe_Vaykor_Cipher extends Application {
    private int[] terminal_access_trackers = new int[4];
    private int[] terminal_difficulty_trackers = new int[4];

    public int get_terminal_access_level(int i) {
        return terminal_access_trackers[i];
    }

    public void set_terminal_access_level(int i, int access_level) {
        this.terminal_access_trackers[i] = access_level;
    }

    public int get_terminal_difficulty_level(int i) {
        return terminal_difficulty_trackers[i];
    }

    public void set_terminal_difficulty_level(int i, int difficulty_level) {
        this.terminal_difficulty_trackers[i] = difficulty_level;
    }
}